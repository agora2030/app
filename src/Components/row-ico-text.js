import React, { Component } from 'react'
import { Thumbnail,  View } from 'native-base'
import { Platform, StyleSheet } from 'react-native'
import Text from './text'

export default class RowIcoText extends Component {
  render(){
    return (
      <View style={[ styles.row, styles.rowItem ]}>
        <View style={ styles.icoThumbnail }>
          <Thumbnail 
            small 
            circle 
            source={ this.props.ico }
            style={ styles.ico }
            />
          <View style={ styles.line } />
        </View>

        <View style={ styles.itemText }>
          <Text 
            numberOfLines={2}
            >
            { this.props.text }
        </Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  rowItem: {
    flexDirection: 'row',
    marginBottom: 10
  },
  icoThumbnail:{
    flex: 1,
    marginLeft: 10
  },
  ico: {
    alignSelf: 'center',
    height: 45,
    width: 45
  },
  line: {
    alignSelf: 'center',
    backgroundColor: '#c5c5c5',
    height: 30,
    position: 'absolute',
    top: 30,
    zIndex: -1,
    ...Platform.select({
      ios: {
        width: 1
      },
      android: {
        width: .5
      }
    })
  },
  itemText: {
    flex: 6,
    justifyContent: 'center',
    marginLeft: 10
  }  
})