import React, { Component } from 'react'
import { Platform, StyleSheet, Text as T } from 'react-native'

export default class Text extends Component {
  render () {
    return (
      <T {...this.props} style={[ styles.f, this.props.style ]}>{ this.props.children }</T>
    )
  }
}

const styles = StyleSheet.create({
  f: {
    color: '#636363',
    fontFamily: 'Titillium Web',
    ...Platform.select({
      android: {
        fontFamily: 'Titillium Web_light'
      }
    })
  }
})
