import React, { Component } from 'react'
import { View, StyleSheet, Platform, Image } from 'react-native'
import Text from './text'
import { connect } from 'react-redux'

const mapStatsToProps = state => (
  {
    userId: state.UserProfile.id
  }
)

class BadgeUserNotifications extends Component {
  componentWillMount () {
    console.log('BadgeUserNotifications', 'userId: ', this.props.userId)
  }
  _renderContent () {
    if (this.props.noIcon) {
      return (
        <View style={[ styles.badge, this.props.style ]}>
          <Text style={styles.count}>{this.props.children}</Text>
        </View>
      )
    }

    return (
      <View style={[ this.props.style, styles.main ]}>
        <Image
          source={require('../Assets/Images/ico_notification.png')}
          style={styles.image}
          />

        <View style={styles.badge}>
          <Text style={styles.count}>{this.props.children}</Text>
        </View>
      </View>
    )
  }
  render () {
    return (
       this._renderContent()
    )
  }
}

const styles = StyleSheet.create({
  main: {
    alignContent: 'center',
    marginTop: -14,
    ...Platform.select({
      android: {
        marginTop: -4
      }
    })
  },
  image: {
    height: 40,
    width: 40,
    marginRight: 5
  },
  badge: {
    borderRadius: 100,
    backgroundColor: 'red',
    paddingVertical: 6,
    paddingHorizontal: 9,
    position: 'absolute',
    right: -12,
    top: -8,
    ...Platform.select({
      android: {
        right: -15
      }
    })
  },
  count: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 11,
    textAlign: 'center'
  }
})

export default connect(mapStatsToProps, null)(BadgeUserNotifications)
