import React, { Component } from 'react'
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import Text from './text'

export default class UserInfoBox extends Component {
  render () {
    const {tracestats, profile} = this.props
    return (
      <View
        style={[ { flexDirection: 'row' }, styles.header ]}
        >
        <View style={{ flexDirection: 'row', flex: 2, marginRight: 20 }}>
          <View style={[styles.boxScore, {marginRight: 10}]}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('UserInitiativesList', { type: 'actionView' })}>
              <View style={styles.score}>
                <Text style={styles.scoreText}>{tracestats.actionView} | 40</Text>
              </View>
              <Text style={styles.note}>INICIATIVAS</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.boxScore}>
            <View style={styles.score}>
              {
                profile.missions
                ? <Text style={styles.scoreText}>{profile.missions.length} | 6</Text>
                : null
              }
            </View>
            <Text style={styles.note}>MISSÕES</Text>
          </View>
        </View>

        <View style={styles.colCoins}>
          <View>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('UserInitiativesList', { type: 'actionFavorite' })}>
              <Image
                source={require('../Assets/Images/favorite.png')}
                style={styles.ico}
                />
              <Text style={styles.note}>{tracestats.actionFavorite} | {(profile.nivel >= 4) ? 15 : 10}</Text>
            </TouchableOpacity>
          </View>
          <View style={{ marginLeft: 6 }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('UserInitiativesList', { type: 'actionDonate' })}>
              <Image
                source={require('../Assets/Images/seeds.png')}
                style={styles.ico}
                />
              <Text style={styles.note}>{profile.coins}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#5d9e5a',
    height: 112,
    marginTop: -30,
    paddingTop: 42,
    paddingHorizontal: 10,
    borderWidth: 0,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20
  },
  boxScore: {
    flex: 1
  },
  score: {
    backgroundColor: 'transparent',
    alignSelf: 'center',
    borderColor: '#fff',
    borderRadius: 26,
    borderWidth: 1,
    paddingVertical: 6,
    paddingHorizontal: 15
  },
  scoreText: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center'
  },
  note: {
    color: '#fff',
    fontSize: 12,
    marginTop: 5,
    textAlign: 'center'
  },
  colCoins: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: -4
  },
  ico: {
    alignSelf: 'center',
    height: 50,
    width: 50,
    marginBottom: -6
  }
})
