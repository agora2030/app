import React, { Component } from 'react'
import {
  View,
  Platform,
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import { Header, Thumbnail } from 'native-base'
// import BadgeUserNotifications from './badge-user-notifications'
import Text from './text'

import * as Progress from 'react-native-progress'

import {getUserAvatar, getUserNivelName, progressBarPercent, userNivelColor} from '../helpers'

export default class UserHeader extends Component {
  constructor (props) {
    super(props)
    this.state = {
      userNameFontSize: 20
    }
  }
  goToProfile () {
    if (!this.props.noLink) {
      this.props.navigation.navigate('Profile')
    }
  }

  render () {
    return (
      <Header
        iosBarStyle='dark-content'
        style={styles.header}
        androidStatusBarColor='#000'
        >
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ width: 70 }}>
            <TouchableOpacity onPress={() => this.goToProfile()}>
              <Thumbnail
                circle
                source={getUserAvatar(this.props.profile)}
                style={[styles.avatar, {borderColor: userNivelColor(this.props.profile.nivel)}]}
              />
            </TouchableOpacity>
          </View>
          <View>
            <Text
              style={[
                styles.nameUser,
                {
                  fontSize: this.state.userNameFontSize
                }
              ]}
              onLayout={e => {
                // console.log(e.nativeEvent.layout.height)
                if (e.nativeEvent.layout.height > 35) {
                  this.setState({ userNameFontSize: 18 })
                }
              }}
            >
              {this.props.profile.firstName} {this.props.profile.lastName}
            </Text>
            <Text style={styles.levelNane}>{getUserNivelName(this.props.profile.nivel)}</Text>
            <Progress.Bar
              color={userNivelColor(this.props.profile.nivel)}
              progress={progressBarPercent(this.props.profile)}
              borderWidth={1}
              borderRadius={10}
              height={4}
              width={100}
              />
          </View>
        </View>
      </Header>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#fff',
    height: 92,
    ...Platform.select({
      ios: {
        paddingTop: 25
      },
      android: {
        paddingTop: 20
      }
    }),
    borderWidth: 0,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20
  },
  avatar: {
    borderWidth: 3,
    borderRadius: 30,
    height: 60,
    width: 60
  },
  nameUser: {
    color: '#595d64',
    fontWeight: 'bold',
    marginTop: 0
  },
  levelNane: {
    color: '#595d64',
    fontSize: 10,
    marginBottom: 3
  }
})
