import React, { Component } from 'react'
import { View } from 'react-native'

export default class Line extends Component {
  render () {
    return (
      <View
        style={{ backgroundColor: '#c5c5c5', height: 0.5, marginVertical: 10 }}
      />
    )
  }
}
