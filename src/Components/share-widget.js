import React, { Component } from 'react'
import Icon from 'react-native-vector-icons/SimpleLineIcons'
import { TouchableOpacity, Share, Platform, StyleSheet, Image } from 'react-native'
import Text from '../Components/text'

export default class ShareWidget extends Component {
  shareMsg () {
    let content = {
      title: this.props.title,
      message: this.props.message,
      url: this.props.url
    }
    if (Platform.OS === 'ios') {
      content.message = `${content.message}.`
    }
    if (Platform.OS === 'android') {
      if (this.props.title) {
        content.message = `${content.message}, "${this.props.title}": ${this.props.url}`
      } else {
        content.message = `${content.message}: ${this.props.url}`
      }
    }
    Share.share(content, { dialogTitle: this.props.title ? `Compartilhando ${this.props.title}` : null })
  }

  render () {
    return (
      <TouchableOpacity onPress={() => this.shareMsg()}>
        <Image
          style={{height: this.props.height ? this.props.height : 50, width: this.props.width ? this.props.width : 50}}
          source={this.props.activeStyle
          ? require('../Assets/Images/actions/actionShare.png')
          : require('../Assets/Images/actions/actionShare_disabled.png')
        }
        />
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  customShare: {
    backgroundColor: '#2c6800',
    width: 56,
    height: 56,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 56,
    marginHorizontal: 5
  }
})
