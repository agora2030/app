import React, { Component } from 'react'
import { View, Platform, StyleSheet,TouchableOpacity } from 'react-native'
import { Label, Item, Input, Picker, Header, Left, Body, Title, Right, Button } from 'native-base'
import mStyles from '../Assets/Styles/Styles'
import Icon from 'react-native-vector-icons/FontAwesome'
import Text from './text'
import H3 from './h3'

export default class InputDate extends Component {
  constructor(props){
    super(props)
    
    this.state = {
      day: [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ],
      month: [ 
        { 'Janeiro': 'Janeiro'  }, 
        { 'Fevereiro': 'Fevereiro' }, 
      ],
      year: [
        1979,
        1980,
        1981
      ]
    }
  }

  render() {
    return (
      <View style={ styles.main }>
        <Button dark transparent style={ styles.close }>
          <Icon name='times' size={ 20 } />
        </Button>
        
        <H3 style={ styles.h3 }>Informe sua data de nascimento</H3>
        <Item regular inlineLabel style={ mStyles.input }> 
          <Label>Dia</Label>
          <Input />
          {/* <Picker
            mode="dialog"
            selectedValue='9'
            placeholder='Selecionar'
            >
              <Picker.Item label={ 'node' } value={9} />
          </Picker> */}
        </Item>

        <Item regular inlineLabel style={ mStyles.input }> 
          <Label>Mês</Label>
          <Input />
        </Item>

        <Item regular inlineLabel style={ mStyles.input }> 
          <Label>Ano</Label>
          <Input />
        </Item>
        
        <Button success rounded style={[ mStyles.buttonPrimary, { alignSelf: 'center' }]}>
          <Text style={[ mStyles.textButtonPrimary, { paddingTop: 10 }]}>Selecionar</Text>
        </Button>
      </View>
    )
  }  
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: '#fff',
    borderRadius: 20,
    padding: 20,
    // height: 180
  },
  close: {
    alignSelf: 'flex-end',

    ...Platform.select({
      ios: {
        marginTop: -5,
        marginBottom: -5
      }
    })
  },
  h3: {
    textAlign: 'center',
    marginBottom: 10,    
  }
})