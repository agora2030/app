
import React, { Component } from 'react'
import { Platform, KeyboardAvoidingView, TouchableOpacity, TextInput, Dimensions } from 'react-native'

import {
  Container,
  Content,
  View,
  Button,
  List,
  ListItem,
  Left,
  Right,
  Body,
  Thumbnail,
  Input,
  Spinner
} from 'native-base'
import { connect } from 'react-redux'
import moment from 'moment'
import Modal from 'react-native-modal'
import Icon from 'react-native-vector-icons/FontAwesome'

import AppHeader from '../../../Components/app-header'
import Text from '../../../Components/text'
import { getComments, createComment, deleteComment, updateComment } from '../../../Api/comment'
import styles from './styles'
import mStyles from '../../../Assets/Styles/Styles'
import { getUserAvatar } from '../../../helpers.js'
import main from 'react-native-youtube';
const { width } = Dimensions.get('window')

const mapStateToProps = state => (
  {
    apiToken: state.UserProfile.apiToken,
    userId: state.UserProfile.id
  }
)

class Comments extends Component {
  constructor (props) {
    super(props)
    this.state = {
      showCommentModal: false,
      updatingComment: false,
      updateCommentIndex: -1,
      updateCommentId: '',
      comments: [],
      isLoading: false,
      page: 2,
      limitInitial: 20,
      skip: 10,
      error: false,
      errorText: '',
      newCommentText: '',
      commentEditText: '',
      disableNewComment: false,
      size: {
        width
      }      
    }
  }
  componentDidMount () {
    this._getComments()
  }
  onKeyboardResigned () {
    this.resetKeyboardView()
  }
  _getComments () {
    this.setState({ isLoading: true })
    getComments(
      this.props.apiToken,
      this.props.navigation.state.params.initiativeId,
      this.state.limitInitial,
      0
    ).then(
        success => {
          this.setState({ isLoading: false, comments: success.data.items })
        },
        error => {
          this.setState({ isLoading: false, error: true, errorText: error.response.data.message })
          console.log(error.response)
        }
      )
  }
  _createComment () {
    this.setState({disableNewComment: true})
    createComment(
      this.props.apiToken,
      this.props.navigation.state.params.initiativeId,
      { text: this.state.newCommentText }
    ).then(
      success => {
        let { comments } = this.state
        comments.push(success.data)
        this.setState({
          disableNewComment: false,
          newCommentText: '',
          comments: comments
        })
      },
      error => {
        console.log(error.response)
        this.setState({disableNewComment: false})
      }
    )
  }
  _getNewComments () {
    const { page, skip } = this.state
    let currentSkip = (page - 1) * skip
    if (currentSkip > 50) {
      currentSkip = 50
    }
    this.setState({ isLoading: true })
    getComments(
      this.props.apiToken,
      this.props.navigation.state.params.initiativeId,
      20,
      currentSkip
    ).then(
        success => {
          const { items } = success.data
          if (items.length > 0) {
            let newData = this.state.comments.concat(items)
            const newPage = this.state.page + 1
            this.setState({
              comments: newData,
              page: newPage,
              limitInitial: (2 * newPage),
              isLoading: false
            })
          } else {
            this.setState({ isLoading: false })
          }
        },
        error => {
          console.log(error.response)
          this.setState({ isLoading: false })
        }
      )
  }
  _deleteComment (commentId, rowID) {
    const { apiToken } = this.props
    const { initiativeId } = this.props.navigation.state.params
    const comments = this.state.comments
    deleteComment(apiToken, initiativeId, commentId)
      .then(
        success => {
          this.setState({ comments: comments.filter(item => item.id !== commentId) })
        },
        error => alert('erro ao apagar comentário')
      )
  }
  _updateComment () {
    const { apiToken } = this.props
    const { initiativeId } = this.props.navigation.state.params
    const { updateCommentIndex, commentEditText, updateCommentId } = this.state
    const comments = [...this.state.comments]
    this.setState({ updatingComment: true })
    updateComment(apiToken, initiativeId, updateCommentId, commentEditText)
      .then(
        success => {
          comments[updateCommentIndex] = success.data
          this.setState({ comments, showCommentModal: false })
          this.setState({ updatingComment: false })
        },
        error => this.setState({ updatingComment: false })
      )
  }
  renderSpinner () {
    if (this.state.isLoading) {
      return (<Spinner width={25} style={{height: 25}} color='#666' />)
    }
  }
  renderComment (item, sectionID, rowID, higlightRow) {
    return (
      <View>
        <ListItem avatar style={styles.noBg}>
          <Left style={{marginTop: 5}}>
            <Thumbnail source={getUserAvatar(item.user)} />
          </Left>
          <Body style={{flexDirection: 'row'}}>
            <Text style={{ fontWeight: 'bold' }}>{item.user.firstName} {item.user.lastName}</Text>
            <Text note> - {moment(item.createdAt).format('h:m A')}</Text>
          </Body>
          <Right />
        </ListItem>

        <ListItem style={styles.noBg}>
          <View style={{ alignItems: 'stretch', width: (width - 30) }}>
            <Text style={{ marginLeft: 70, marginTop: -10 }}>{item.text}</Text>
            
            <View>
            {
            item.user.id === this.props.userId
              ? (
                <View
                  style={{
                    marginBottom: -15,
                    marginVertical: 10,
                    paddingBottom: 10,
                    flexDirection: 'row',
                    alignItems: 'stretch',
                    alignSelf: 'flex-end'
                  }}
                >
                  <TouchableOpacity
                    onPress={
                      () => this.setState({
                        showCommentModal: true,
                        commentEditText: item.text,
                        updateCommentIndex: rowID,
                        updateCommentId: item.id
                      })
                    }
                    style={{ paddingVertical: 3, paddingHorizontal: 6, marginRight: 5 }}
                  >
                    <Icon name='edit' color='#696969' size={16} />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => this._deleteComment(item.id, rowID)}
                    style={{ paddingVertical: 3, paddingHorizontal: 6 }}
                  >
                    <Icon name='trash' color='#696969' size={16} />
                  </TouchableOpacity>
                </View>
              )
              : null
            }            
            </View>
          </View>
        </ListItem>
      </View>
    )
  }

  keyboardAccessoryViewContent () {
    return (
      <View style={styles.inputContainer}>
        <Input
          style={styles.textInput}
          placeholder='Deixe seu comentário...'
          defaultValue={this.state.newCommentText}
          onChangeText={v => this.setState({ newCommentText: v })}
          multiline
          disabled={this.state.disableNewComment}
        />

        <View>
          <Button
            transparent
            small
            onPress={() => this._createComment()}
            disabled={this.state.disableNewComment}
          >
            <Icon name='send' color='#61a05e' size={22} style={{ padding: 8, marginLeft: 10 }} />
          </Button>
        </View>
      </View>
    )
  }

  render () {
    const { showCommentModal, commentEditText, updatingComment } = this.state
    if (this.state.error) {
      return (<Text>Erro ao obter comentários: {this.state.errorText}</Text>)
    }
    return (
      <Container style={styles.container}>
        <AppHeader {...this.props} title='Comentários' backButton />
        <Content>
          <List
            dataArray={this.state.comments}
            renderRow={this.renderComment.bind(this)}
            keyExtractor={(item, index) => index}
            onEndReached={this._getNewComments.bind(this)}
          />

          <View style={{ height: 30, marginTop: 20 }}>{this.renderSpinner()}</View>
          <Modal
            isVisible={showCommentModal}
            onBackdropPress={() => this.setState({ showCommentModal: false })}
            onBackButtonPress={() => this.setState({ showCommentModal: false })}
          >
            <View style={{ backgroundColor: '#fff', padding: 20, borderRadius: 20 }}>
              <TextInput
                value={commentEditText}
                onChangeText={v => this.setState({ commentEditText: v })}
                placeholder='digite seu comentário'
                multiline
                autoGrow
                underlineColorAndroid='transparent'
                style={{ fontSize: 16 }}
              />
              {
                updatingComment
                  ? (
                    <Spinner width={15} style={{height: 15, marginTop: 42}} color='#666' />
                  )
                  : (
                    <TouchableOpacity
                      onPress={() => {
                        this._updateComment()
                      }}
                      disabled={commentEditText.length === 0}
                      style={[
                        mStyles.buttonPrimary,
                        {
                          borderRadius: 6,
                          marginTop: 20,
                          paddingVertical: 10,
                          alignItems: 'center'
                        }
                      ]}
                    >
                      <Text style={[ mStyles.textButtonPrimary, { backgroundColor: 'transparent' }]}>Atualizar</Text>
                    </TouchableOpacity>
                  )
              }
            </View>
          </Modal>
        </Content>

        <KeyboardAvoidingView
          behavior={(Platform.OS === 'android') ? 'height' : 'position'}
          style={styles.inputComment}
          >
          { this.keyboardAccessoryViewContent() }
        </KeyboardAvoidingView>
      </Container>
    )
  }
}

export default connect(mapStateToProps, null)(Comments)
