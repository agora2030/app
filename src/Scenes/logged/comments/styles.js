import { Platform, StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e6ebee'
  },
  noBg: {
    backgroundColor: 'transparent'
  },
  inputComment: {
    ...Platform.select({
      android: {
        paddingBottom: 10
      }
    })
  },
  inputContainer: {
    // height: 150,
    borderTopColor: '#d9d9d9',
    borderTopWidth: 1,
    backgroundColor: '#f7f7f7',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 8
  },
  textInput: {
    backgroundColor: 'white',
    borderWidth: 0.5,
    borderRadius: 20,
    minHeight: 80,
    maxHeight: 160,
    borderColor: '#ddd',
    fontSize: 16,
    paddingLeft: 10,
    paddingRight: 10,
    paddingVertical: 6
  },
  sendButton: {
    paddingHorizontal: 15
  }
})
