import React, { Component } from 'react'
import { View, Image, TouchableOpacity } from 'react-native'
import { Spinner, ListItem, CheckBox, Toast, List, Button } from 'native-base'
import Modal from 'react-native-modal'
import Text from '../../../../Components/text'
import H3 from '../../../../Components/h3'
import { like } from '../../../../Api/initiativeActions'
import mStyles from '../../../../Assets/Styles/Styles'
import Icon from 'react-native-vector-icons/FontAwesome'

export default class ActionLike extends Component {
  constructor (props) {
    super(props)
    this.state = {
      actionLike: this.props.actionLike,
      actionLikeShowModal: false,
      actionLikeDisbleModalBtnConfirm: true,
      actionLikeModalConfirmLoading: false,
      likeTerms: {
        Relevante: false,
        Replicável: false,
        Inovadora: false,
        Participativa: false,
        Criativa: false,
        Fascinante: false
      }
    }
  }

  _likeButtonRender () {
    if (this.state.actionLike) {
      return (
        <Image
          style={{height: 50, width: 50}}
          source={require('../../../../Assets/Images/actions/actionLike.png')}
        />
      )
    } else {
      return (
        <TouchableOpacity onPress={() => this.setState({ actionLikeShowModal: true })}>
          <Image
            style={{height: 50, width: 50}}
            source={require('../../../../Assets/Images/actions/actionLike_disabled.png')}
          />
        </TouchableOpacity>
      )
    }
  }

  _updateLikeTerms (label) {
    const terms = this.state.likeTerms
    terms[label] = !terms[label]
    const termsActive = Object.keys(terms).filter(key => terms[key])
    this.setState({likeTerms: terms, actionLikeDisbleModalBtnConfirm: (!termsActive.length > 0)})
  }

  _confirmLike () {
    const terms = Object.keys(this.state.likeTerms).filter(key => this.state.likeTerms[key])
    this.setState({actionLikeDisbleModalBtnConfirm: true})
    like(this.props.initiativeId, terms, this.props.apiToken)
    .then(
      success => {
        this.setState({
          actionLike: true,
          actionLikeModalConfirmLoading: false,
          actionLikeShowModal: false
        })
      },
      error => {
        console.log('Erro no like: ', error, this.props.initiativeId)
        this.setState({
          actionLikeModalConfirmLoading: false,
          actionLikeShowModal: false
        })
        Toast.show({
          text: 'Houve um erro na ação. =|',
          position: 'bottom',
          buttonText: 'Ok',
          duration: 4000
        })
      }
    )
  }

  render () {
    return (
      <View>
        {this._likeButtonRender()}
        <Modal
          isVisible={this.state.actionLikeShowModal}
          onBackdropPress={() => { this.setState({actionLikeShowModal: false}) }}
          style={{ alignSelf: 'center', margin: 30, width: 250 }}
        >
          <View style={{ backgroundColor: '#fff', borderRadius: 15, padding: 20 }}>
            <Button
              transparent
              small
              style={mStyles.modalBtClose}
              onPress={() => { this.setState({ actionLikeShowModal: false }) }}
              >
              <Icon name='times' size={20} style={mStyles.modalIcoClose} />
            </Button>

            <H3 style={{textAlign: 'center'}}>Gostou porquê:</H3>
            {this.state.actionLikeModalConfirmLoading ? <Spinner width={35} color='#666' /> : null}
            <List>
              {
                Object.keys(this.state.likeTerms).map((item, index) => {
                  return (
                    <ListItem
                      key={index}
                      onPress={() => { this._updateLikeTerms(item) }}
                      style={{ marginHorizontal: 0 }}
                    >
                      <CheckBox
                        color='#52a750'
                        checked={this.state.likeTerms[item]}
                        onPress={() => this._updateLikeTerms(item)}
                      />
                      <Text style={{ marginLeft: 10 }}>{item}</Text>
                    </ListItem>
                  )
                })
              }
            </List>
            <View style={{ alignSelf: 'center' }}>
              <Button
                rounded
                onPress={() => {
                  this.setState({ actionLikeModalConfirmLoading: true })
                  this._confirmLike()
                }}
                disabled={this.state.actionLikeDisbleModalBtnConfirm}
                style={[
                  this.state.actionLikeDisbleModalBtnConfirm ? mStyles.btnDisabled : mStyles.buttonPrimary,
                  { paddingHorizontal: 20 }
                ]}
                >
                <Text style={this.state.actionLikeDisbleModalBtnConfirm ? mStyles.textButtonPrimaryDisabled : mStyles.textButtonPrimary}>Enviar</Text>
              </Button>
            </View>
          </View>
        </Modal>
      </View>
    )
  }
}
