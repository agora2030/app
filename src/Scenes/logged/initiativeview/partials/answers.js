import React, { Component } from 'react'
import { View, StyleSheet, ActivityIndicator } from 'react-native'
import Text from '../../../../Components/text'

export default class AnswersList extends Component {
  render () {
    const {
      isLoading,
      showAnswerIndex,
      excludeAnswerIndex,
      answers
    } = this.props
    if (isLoading) {
      return (<ActivityIndicator size={'large'} color='#666' />)
    }
    if (showAnswerIndex === 0) {
      const a = answers[this.props.showAnswerIndex]
      return (
        <View style={styles.view}>
          <Text style={{ fontWeight: 'bold' }}>{a.question.title}</Text>
          <Text>{a.text}</Text>
        </View>
      )
    }
    if (typeof excludeAnswerIndex === 'number') {
      return (
        <View style={styles.view}>
          {answers.map((a, index) => {
            if (excludeAnswerIndex !== index) {
              return (
                <View key={index} style={{ marginVertical: 10 }}>
                  <Text style={{ fontWeight: 'bold' }}>{a.question.title}</Text>
                  <Text>{a.text}</Text>
                </View>
              )
            }
          })}
        </View>
      )
    }
    return (
      <View style={styles.view}>
        {answers.map((a, index) => {
          return (
            <View key={index} style={{ marginVertical: 10 }}>
              <Text style={{ fontWeight: 'bold' }}>{a.question.title}</Text>
              <Text>{a.text}</Text>
            </View>
          )
        })}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  view: {
    marginHorizontal: 20,
    marginBottom: 15
  }
})
