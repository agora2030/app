import React, { Component } from 'react'
import { View, TouchableOpacity, Image } from 'react-native'
import { Text, Button } from 'native-base'
import styles from '../styles'
import mStyles from '../../../../Assets/Styles/Styles'
import Modal from 'react-native-modal'
import H3 from '../../../../Components/h3'

export default class Ods extends Component {
  constructor (props) {
    super(props)
    this.state = {
      showModal: false,
      selectedOds: {}
    }
  }

  getOdsIcon (number) {
    switch (number) {
      case 1:
        return require('../../../../Assets/Images/ods/1.png')
      case 2:
        return require('../../../../Assets/Images/ods/2.png')
      case 3:
        return require('../../../../Assets/Images/ods/3.png')
      case 4:
        return require('../../../../Assets/Images/ods/4.png')
      case 5:
        return require('../../../../Assets/Images/ods/5.png')
      case 6:
        return require('../../../../Assets/Images/ods/6.png')
      case 7:
        return require('../../../../Assets/Images/ods/7.png')
      case 8:
        return require('../../../../Assets/Images/ods/8.png')
      case 9:
        return require('../../../../Assets/Images/ods/9.png')
      case 10:
        return require('../../../../Assets/Images/ods/10.png')
      case 11:
        return require('../../../../Assets/Images/ods/11.png')
      case 12:
        return require('../../../../Assets/Images/ods/12.png')
      case 13:
        return require('../../../../Assets/Images/ods/13.png')
      case 14:
        return require('../../../../Assets/Images/ods/14.png')
      case 15:
        return require('../../../../Assets/Images/ods/15.png')
      case 16:
        return require('../../../../Assets/Images/ods/16.png')
      case 17:
        return require('../../../../Assets/Images/ods/17.png')
      default:
        return require('../../../../Assets/Images/ods.png')
    }
  }

  _renderModalContent () {
    return (
      <View style={{ backgroundColor: '#fff', borderRadius: 15, padding: 20 }}>
        <Image source={this.getOdsIcon(this.state.selectedOds.number)}
          style={{width: 120, height: 120, alignSelf: 'center'}} />
        <Text style={{textAlign: 'center', marginVertical: 10}}>
          {this.state.selectedOds.description}
        </Text>
        <View style={{ alignSelf: 'center' }}>
          <Button rounded success
            onPress={() => { this.setState({ showModal: false }) }}
            style={mStyles.buttonPrimary}
            >
            <Text style={[mStyles.textButtonPrimary, {textAlign: 'center', width: 150}]}>Ok</Text>
          </Button>
        </View>
      </View>
    )
  }

  render () {
    const {ods} = this.props
    return (
      <View style={{marginBottom: 10, alignSelf: 'center'}}>
        <View style={[ styles.row, {alignItems: 'center'} ]}>
          <Image source={require('../../../../Assets/Images/ods.png')} style={styles.ico} />
          {
            ods.map((ods, index) => (
              <TouchableOpacity
                onPress={() => { this.setState({ showModal: true, selectedOds: ods }) }}
                style={[mStyles.ods, { backgroundColor: ods.color, paddingVertical: 5 }]} key={index} >
                <Text style={mStyles.odsId}>{ods.number}</Text>
              </TouchableOpacity>
            ))
          }
        </View>

        <Text note style={{ textAlign: 'center', color: 'rgba(0,0,0,.5)' }}>Toque nas ods e saiba mais</Text>
        <Modal
          isVisible={this.state.showModal}
          onBackdropPress={() => { this.setState({showModal: false}) }}
          style={{alignSelf: 'center', margin: 30, width: 250}}
        >
          {this._renderModalContent()}
        </Modal>
      </View>
    )
  }
}
