import React, { Component } from 'react'
import { View, Image, TouchableOpacity } from 'react-native'
import { Text, Spinner, Toast, Button } from 'native-base'
import Modal from 'react-native-modal'
import H3 from '../../../../Components/h3'
import { donate } from '../../../../Api/initiativeActions'
import Icon from 'react-native-vector-icons/FontAwesome'
import mStyles from '../../../../Assets/Styles/Styles'

export default class ActionDonate extends Component {
  constructor (props) {
    super(props)
    this.state = {
      actionDonate: this.props.actionDonate,
      actionDonateShowModal: false,
      actionDonateModalConfirmLoading: false,
      nivel: this.props.nivel,
      coins: this.props.coins,
      valueDonate: 10,
      actionDonateDisbleModalBtnConfirm: false
    }
  }

  _buttonRender () {
    if (this.state.coins <= 0 || this.state.nivel < 4) {
      return (
        <TouchableOpacity onPress={() => this.setState({ actionDonateShowModal: true })}>
          <Image
            style={{height: 50, width: 50}}
            source={require('../../../../Assets/Images/actions/actionDonate_blocked.png')}
          />
        </TouchableOpacity>
      )
    } else if (this.state.actionDonate) {
      return (
        <Image
          style={{height: 50, width: 50}}
          source={require('../../../../Assets/Images/actions/actionDonate.png')}
        />
      )
    } else {
      return (
        <TouchableOpacity onPress={() => this.setState({ actionDonateShowModal: true })}>
          <Image
            style={{height: 50, width: 50}}
            source={require('../../../../Assets/Images/actions/actionDonate_disabled.png')}
          />
        </TouchableOpacity>
      )
    }
  }

  _incrementDonate () {
    let maxValue = (this.state.coins > 50) ? 50 : this.state.coins
    let actualDonate = this.state.valueDonate + 5
    this.setState({ valueDonate: (actualDonate > maxValue) ? maxValue : actualDonate })
  }

  _decrementDonate () {
    let minValue = 5
    let actualDonate = this.state.valueDonate - 5
    this.setState({ valueDonate: (actualDonate < minValue) ? minValue : actualDonate })
  }

  _confirm () {
    this.setState({actionDonateDisbleModalBtnConfirm: true})
    donate(this.props.initiativeId, this.state.valueDonate, this.props.apiToken)
    .then(
      success => {
        this.setState({
          actionDonate: true,
          actionDonateModalConfirmLoading: false,
          actionDonateShowModal: false,
          actionDonateDisbleModalBtnConfirm: false
        })
      },
      error => {
        console.log('Erro ao Apoiar: ', error, this.props.initiativeId)
        this.setState({
          actionDonateModalConfirmLoading: false,
          actionDonateDisbleModalBtnConfirm: false,
          actionDonateShowModal: false
        })
        Toast.show({
          text: 'Houve um erro na ação. =|',
          position: 'bottom',
          buttonText: 'Ok',
          duration: 4000
        })
      }
    )
  }

  _modalContent () {
    if (this.state.coins <= 0) {
      return (
        <View style={{ backgroundColor: '#fff', borderRadius: 15, padding: 20 }}>
          <H3 style={{textAlign: 'center'}}>Apoiar</H3>
          <Text style={{textAlign: 'center'}}>
            Você não possui saldo para apoiar essa initiativa. =|
          </Text>
          <Text style={{ textAlign: 'center' }}>Saldo Atual: {this.state.coins}</Text>
          <Button transparent full onPress={() => { this.setState({ actionDonateShowModal: false }) }}>
            <Text style={{ marginTop: 4 }}>Ok</Text>
          </Button>
        </View>
      )
    } else if (this.state.nivel < 4) {
      return (
        <View style={{ backgroundColor: '#fff', borderRadius: 15, padding: 20 }}>
          <H3 style={{textAlign: 'center'}}>Apoiar</H3>
          <Text style={{textAlign: 'center', marginVertical: 10 }}>
            Você precisa estar no Nível País para apoiar uma iniciativa. Continue jogando e logo logo você chega lá :)
          </Text>
          <View style={{ alignSelf: 'center' }}>
            <Button
              rounded
              success
              onPress={() => { this.setState({ actionDonateShowModal: false }) }}
              style={mStyles.buttonPrimary}
                >
              <Text style={[ mStyles.textButtonPrimary, { textAlign: 'center', marginTop: 5, width: 150}]}>Ok</Text>
            </Button>
          </View>
        </View>
      )
    } else {
      return (
        <View style={{ backgroundColor: '#fff', borderRadius: 15, padding: 20 }}>
          <Button
            transparent
            small
            style={mStyles.modalBtClose}
            onPress={() => { this.setState({ actionDonateShowModal: false }) }}
            >
            <Icon name='times' size={20} style={mStyles.modalIcoClose} />
          </Button>
          <H3 style={{textAlign: 'center'}}>Apoiar</H3>
          {this.state.actionDonateModalConfirmLoading ? <Spinner width={35} color='#666' /> : null}
          <Text style={{textAlign: 'center'}}>
            Quantos Seeds ?
          </Text>
          <View style={{justifyContent: 'center', flexDirection: 'row', alignItems: 'center'}}>
            <Button transparent full onPress={() => { this._decrementDonate() }}>
              <Icon name='minus' size={30} />
            </Button>
            <Text style={{
              textAlignVertical: 'center', fontWeight: 'bold', textAlign: 'center', fontSize: 40, marginHorizontal: 30}}>
              {this.state.valueDonate}
            </Text>
            <Button transparent full onPress={() => { this._incrementDonate() }}>
              <Icon name='plus' size={30} />
            </Button>
          </View>
          <Text>Saldo Atual: {this.state.coins}</Text>
          <View style={{ alignSelf: 'center' }}>
            <Button
              rounded
              success
              onPress={() => {
                this.setState({ actionDonateModalConfirmLoading: true })
                this._confirm()
              }}
              disabled={this.state.actionDonateDisbleModalBtnConfirm}
              style={mStyles.buttonPrimary}
                >
              <Text style={[mStyles.textButtonPrimary, {textAlign: 'center', width: 150}]}>Apoiar</Text>
            </Button>
          </View>
        </View>
      )
    }
  }

  render () {
    return (
      <View>
        {this._buttonRender()}
        <Modal
          isVisible={this.state.actionDonateShowModal}
          onBackdropPress={() => { this.setState({actionDonateShowModal: false}) }}
          style={{ alignSelf: 'center', margin: 30, width: 250 }}
         >
          {this._modalContent()}
        </Modal>
      </View>
    )
  }
}
