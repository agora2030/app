import React, { Component } from 'react'
import { View, TouchableOpacity } from 'react-native'
import { Thumbnail } from 'native-base'
import styles from '../styles'
import mStyles from '../../../../Assets/Styles/Styles'
import H3 from '../../../../Components/h3'
import { getUserAvatar } from '../../../../helpers'
export default class WhoLikes extends Component {
  renderWhoLikes () {
    return (
      <View style={{ marginTop: 10 }} padder>
        <H3 style={{ textAlign: 'center', marginBottom: 10 }}>Quem Gostou</H3>
        <View style={[mStyles.contentCenter, styles.row, { alignItems: 'center' }]}>
          {
            this.props.whoLikes.map((usr, index) => (
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Profile', { uid: usr.id })}
                key={index}
              >
                <Thumbnail style={{ marginRight: 8 }} small source={getUserAvatar(usr)} />
              </TouchableOpacity>
            ))
          }
          {(this.props.whoLikesQnt > 0) ? <H3> +{this.props.whoLikesQnt} </H3> : null}
        </View>

      </View>
    )
  }

  render () {
    return (
      <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
        {this.props.whoLikes.length > 0 ? this.renderWhoLikes() : null}
      </View>
    )
  }
}
