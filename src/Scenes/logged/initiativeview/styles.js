import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c9cbd0'
  },
  row: {
    flexDirection: 'row'
  },
  tag: {
    backgroundColor: '#4b9f46',
    paddingVertical: 5,
    paddingHorizontal: 12, 
    borderRadius: 16, 
    marginRight: 5, 
  },
  contentMargin: {
    marginHorizontal: 20
  },
  title: { 
    color: '#4ca047',     
    textAlign:'center', 
    fontWeight: 'bold', 
    fontSize: 23,
    marginVertical: 15
  },
  rowLabelText: { 
    alignContent: 'space-between'
  },
  ico: { 
    marginRight: 10,
    height: 40,
    width: 40
  },
  rowLeanMore: { 
    justifyContent: 'center', 
    marginBottom: 10
  },
  buttonPrimary: {
    height: 60,
    paddingHorizontal: 15
  },
  textButtonPrimary: { 
    color: '#4ca047', 
    fontSize: 17,
    fontWeight: 'bold' 
  }
})