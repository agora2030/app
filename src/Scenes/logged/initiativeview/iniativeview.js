import React, { Component } from 'react'
import { connect } from 'react-redux'
import YouTube from 'react-native-youtube'
import Icon from 'react-native-vector-icons/FontAwesome'
import AppHeader from '../../../Components/app-header'
import Text from '../../../Components/text'
import H2 from '../../../Components/h2'
import H3 from '../../../Components/h3'
import Line from '../../../Components/line'
import mStyles from '../../../Assets/Styles/Styles'
import styles from './styles'
import Answers from './partials/answers'
import ImageCarousel  from './partials/imagecarousel'
import Ods  from './partials/ods'
import Informations from './partials/informations'
import WhoLikes from './partials/whoLikes'
import ActionLike from './partials/actionLike'
import ActionFavorite from './partials/actionFavorite'
import ActionDonate from './partials/actionDonate'
import ActionShare from './partials/actionShare'
import { youtubeParser } from '../../../helpers'
import { getProfile } from '../../../Api/profile'
import { pageHost } from '../../../Api/api.variables'
import { Image, Dimensions, TouchableOpacity} from 'react-native'
import { Container, Content, View, Button, Spinner, Right, Left} from 'native-base'
import { getInitiative, getInitiativeMedias, getInitiativeAnswers, getInitiativeUrls } from '../../../Api/initiative'
import Communications from 'react-native-communications'

const mapStateToProps = state => (
  {
		apiToken: state.UserProfile.apiToken,
		tracestats: state.Tracestats
  }
)

const { width, height } = Dimensions.get('window')
const isCollapsed = true;

class InitiativeDetails extends Component {
	constructor(props) {
		super(props)
		this.state = {
			size: { width, height },
			isCollapsed: true,
			initiative: {},
      showLoading: true,
      error: false,
			errorText: '',
			answers: [],
			images: [],
			actions: {
				actionLike: false,
				actionView: true,
				actionShare: false,
				actionDonate: false,
				actionFavorite: false,
				actionComment: false
			},
			youtubeId: '',
			youtubeHeight: 239,
			loadingAnswers: true,
			loadingMediaImage: true,
			loadingUrlYoutube: true,
			loadingActions: true,
			profile: {}
		}
	}

	componentWillMount () {
		const { id } = this.props.navigation.state.params
		const { apiToken } = this.props.apiToken
		this._getInitiative(apiToken, id)

		getProfile('resume', apiToken)
    .then(
      success => {
        this.setState({ profile: success.data, loadingActions: false })
      },
      error => {
        console.log('erro ao obter perfil: ', error.response)
			}
		)

		getInitiativeAnswers(apiToken, id)
		.then(
			success => {
				this.setState({ loadingAnswers: false, answers: success.data.items })
			},
			error => {
				console.log('erro ao obter respostas: ', error.response)
				this.setState({ loadingAnswers: false })
			}
		)
		
		getInitiativeMedias(apiToken, id, 'image')
		.then(
			success => {
				this.setState({ loadingMediaImage: false, images: success.data.items })
			},
			error => {
				console.log('erro ao obter medias: ', error.response)
				this.setState({ loadingMediaImage: false })
			}
		)
		
		getInitiativeUrls(apiToken, id, 'video')
		.then(
			success => {
				this.setState({ loadingUrlYoutube: false, youtubeId: youtubeParser(success.data.items)})						
			},
			error => {
				console.log('erro ao obter video: ', error.response)
				this.setState({ loadingUrlYoutube: false })
			}
		)
	}
	
  _getInitiative (token, id) {
    getInitiative(token, id)
    .then(
      success => {
        this.setState({initiative: success.data, actions: success.data.actions,  showLoading: false})
      },
      error => {
        this.setState({showLoading: false, error: true, errorText: error.response.data.message})
      }
    )
  }

	_onLayoutDidChange = (e) => {
    const layout = e.nativeEvent.layout
    this.setState({ size: { width: layout.width, height: 100 } })
	}
		
	_renderYT () {
		if (this.state.youtubeId.length > 0) {
			return (
				<View style={{ marginVertical: 30}}>
					<YouTube
						videoId={this.state.youtubeId}
						play={false}
						fullscreen={false}
						loop={false}
						onReady={e => setTimeout(() => this.setState({ isReady: true, youtubeHeight: 240 }), 200)}
						onChangeState={e => this.setState({ status: e.state })}
						onChangeQuality={e => this.setState({ quality: e.quality })}
						onError={e => this.setState({ error: e.error })}
						style={{ alignSelf: 'stretch', height: this.state.youtubeHeight }}
						apiKey='115773860068-vp0a144j8tc7n4jqstnfca2fr4n5sjaa.apps.googleusercontent.com'
						/>							
				</View>
			)
		}
	}

	_renderActions(){
		if(this.state.loadingActions){
			return (<Spinner width={35} color='#666' />)
		}
		return(
			<View style={ [ mStyles.contentCenter, styles.row, { marginVertical: 10, marginBottom:15}] }>
				<Image
					style={{height: 50, width: 50}}
					source={require('../../../Assets/Images/actions/actionView.png')}
				/>
				<ActionLike 
					actionLike={this.state.actions.actionLike}
					initiativeId={this.props.navigation.state.params.id}
					apiToken={this.props.apiToken}
				/>
				<ActionFavorite 
					actionFavorite={this.state.actions.actionFavorite}
					initiativeId={this.props.navigation.state.params.id}
					apiToken={this.props.apiToken}
					favorites={this.state.profile.favorites}
				/>
				<ActionDonate 
					actionDonate={this.state.actions.actionDonate}
					initiativeId={this.props.navigation.state.params.id}
					apiToken={this.props.apiToken}
					nivel={this.state.profile.nivel}
					coins={this.state.profile.coins}
				/>
				<ActionShare 
					actionShare={this.state.actions.actionShare}
					initiativeId={this.props.navigation.state.params.id}
					apiToken={this.props.apiToken}
					slug={this.state.initiative.slug}
					title={this.state.initiative.title}
				/>
				<TouchableOpacity onPress={() => this.props.navigation.navigate('Comments', { initiativeId: this.state.initiative.id })}>
					<Image 
						style={{height: 50, width: 50}}
						source={(this.state.actions.actionComment)
						? require('../../../Assets/Images/actions/actionComment.png')
						: require('../../../Assets/Images/actions/actionComment_disabled.png')}
					/>
				</TouchableOpacity>	
			</View>
		)
	}

  render() {
		if (this.state.showLoading) {
      return (<Spinner width={35} color='#666' />)
    }
		
		if (this.state.error) {
      return (
				<Container style={ styles.container }>
					<AppHeader {...this.props} title='Iniciativa' backButton eventId='updateInitiativesList' />
					<Content padder>
						<Text>Erro ao obter iniciativa: {this.state.errorText}</Text>
					</Content>
				</Container>
			)
    }
		
		return (
      <Container style={ {backgroundColor: '#e6ebee'} }>
        <AppHeader {...this.props} title='Iniciativa' backButton eventId='updateInitiativesList' />
        <Content>
					<View>
						
						<View>
							<Image source={{ uri: this.state.initiative.coverImage.urls['570x370'] }} style={{height: 300, width: null, flex: 1}}/>
						</View>
						
						<Informations
							initiative={this.state.initiative}
						/>

						<Answers
							isLoading={this.state.loadingAnswers}
							showAnswerIndex={0}
							answers={this.state.answers}
						/>

						<ImageCarousel
							size={this.state.size}
							isLoading={this.state.loadingMediaImage}
							images={this.state.images}
							zoom
							navigation={this.props.navigation}
						/>

						<Answers
							isLoading={this.state.loadingAnswers}
							excludeAnswerIndex={0}
							answers={this.state.answers}
						/>

						{this._renderYT()}

						<H2 style={{textAlign: 'center', marginVertical: 5}}>Ações de Curadoria</H2>
						
						{this._renderActions()}

						<View style={[ styles.row, styles.rowLeanMore ]}>
							<Button bordered rounded success style={ styles.buttonPrimary }
								onPress={() => {Communications.web(`${pageHost}/initiative/${this.state.initiative.slug}`)}}
							>
								<Image
									style={ styles.ico } 
									source={require('../../../Assets/Images/actions/actionView_disabled.png')}
								/>
								<Text style={ styles.textButtonPrimary }>Ler mais sobre a iniciativa</Text>
							</Button>
						</View>
						
						<Line />

						<WhoLikes
							{...this.props}
							whoLikes={this.state.initiative.whoLikes}
							whoLikesQnt={this.state.initiative.whoLikesQnt}
						/>
				
					</View>   
        </Content>
      </Container>
    )
  }
}

export default connect(mapStateToProps, null)(InitiativeDetails)
