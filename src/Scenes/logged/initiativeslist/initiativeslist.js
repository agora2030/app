import React, { Component } from 'react'
import {
  Image, View, TouchableOpacity,
  DeviceEventEmitter, RefreshControl
} from 'react-native'
import {
  Card, CardItem, Spinner, List, Input,
  Item, Button
} from 'native-base'
import { getInitiatives } from '../../../Api/initiative'
import { getOds } from '../../../Api/ods'
import { connect } from 'react-redux'
import Text from '../../../Components/text'
import mStyles from '../../../Assets/Styles/Styles'
import styles from './styles'
import AppHeader from '../../../Components/app-header'

const mapStateToProps = state => (
  {
    apiToken: state.UserProfile.apiToken,
    fullProfile: state.UserProfile
  }
)

class InitiativesList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      initiatives: [],
      showLoading: true,
      showLoadingMore: false,
      refreshing: false,
      error: false,
      errorText: '',
      page: 2,
      limitInitial: 50,
      skip: 0,
      Ods: [],
      scope: '',
      filter: { q: '', ods: '0' },
      clearFilter: false,
      showSearch: true, // trocar para false quando precisar dos filtros
      showSearchBtn: true,
      loadingSearch: false
    }
  }

  componentWillMount () {
    const { apiToken } = this.props
    this._getInitiatives(apiToken, 2)
    DeviceEventEmitter.addListener('updateInitiativesList', e => {
      this.setState({ showLoading: true, initiatives: [] })
      this._getInitiatives(this.props.apiToken)
    })
    getOds(apiToken)
      .then(
        success => {
          this.setState({ Ods: this.state.Ods.concat(success.data.items) })
        },
        error => {
          console.log(error.response)
          if (error.response.status === 401) {
            DeviceEventEmitter.emit('doLogout', { expiredSession: true })
          }
        }
      )
  }

  refreshInitiatives () {
    this.setState({ refreshing: true })
    const { limitInitial, scope, filter } = this.state
    getInitiatives(this.props.apiToken, limitInitial, 0, scope, filter)
      .then(
        success => {
          this.setState({initiatives: success.data.items, refreshing: false})
        },
        error => {
          console.log(error.response)
          this.setState({refreshing: false, error: true, errorText: error.response.data.message})
        }
      )
  }

  _getInitiatives (token, resetFilter) {
    let { limitInitial, scope, filter } = this.state
    if (resetFilter) {
      filter = ''
    }
    getInitiatives(token, limitInitial, 0, scope, filter)
    .then(
      success => {
        this.setState({initiatives: success.data.items, loadingSearch: false, showLoading: false})
      },
      error => {
        console.log(error.response)
        this.setState({showLoading: false, loadingSearch: false, error: true, errorText: error.response.data.message})
        if (error.response.status === 401) {
          DeviceEventEmitter.emit('doLogout', { expiredSession: true })
        }
      }
    )
  }

  getNextPage () {
    const { page, skip, scope, filter } = this.state
    let currentSkip = (page - 1) * skip
    if (currentSkip > 50) {
      currentSkip = 50
    }
    this.setState({ showLoadingMore: true })
    getInitiatives(this.props.apiToken, 10, currentSkip, scope, filter)
    .then(
      success => {
        const { items } = success.data
        if (items.length > 0) {
          let newData = this.state.initiatives.concat(items)
          const newPage = this.state.page + 1
          this.setState({
            initiatives: newData,
            page: newPage,
            limitInitial: (2 * newPage),
            showLoadingMore: false
          })
        } else {
          this.setState({ showLoadingMore: false })
        }
      },
      error => {
        console.log(error.response)
        this.setState({error: true, errorText: error.response.data.message, showLoadingMore: false})
      }
    )
  }

  renderNextItemsSpinner () {
    if (this.state.showLoadingMore) {
      return (<Spinner width={25} style={{height: 25}} color='#666' />)
    }
  }

  renderRow (item) {
    const { navigate } = this.props.screenProps.rootNav
    return (
      <TouchableOpacity
        onPress={() => navigate('InitiativeView', {id: item.id})}
      >
        <Card style={styles.card}>
          <CardItem>
            <Image
              source={{uri: item.coverImage['570x370']}}
              style={styles.thumbnail}
              />
          </CardItem>

          <CardItem>
            <Text style={styles.title}>{item.title}</Text>
          </CardItem>

          <CardItem footer>
            <View style={[ styles.row, styles.rowIcos, { flex: 1, marginTop: -5 } ]}>
              {
                item.ods.map((ods, index) => (
                  <View style={[ mStyles.ods, { backgroundColor: ods.color } ]} key={index} >
                    <Text style={mStyles.odsId}>{ods.number}</Text>
                  </View>
              ))
              }

              <View style={styles.icoView}>
                <Image
                  source={(item.visualized)
                  ? require('../../../Assets/Images/actions/actionView.png')
                  : require('../../../Assets/Images/actions/actionView_disabled.png')}
                  style={styles.ico}
                  />
              </View>
            </View>
          </CardItem>
        </Card>
      </TouchableOpacity>)
  }

  setFilterQ (v) {
    let f = {
      q: v,
      ods: this.state.filter.ods
    }
    this.setState({ filter: f })
  }

  setFilterOds (v) {
    let f = {
      q: this.state.filter.q,
      ods: v
    }
    this.setState({ filter: f })
  }

  doSearch () {
    if (this.state.filter.q || this.state.filter.ods) {
      const { apiToken } = this.props
      this.setState({ loadingSearch: true, clearFilter: true })
      this._getInitiatives(apiToken)
    }
  }

  doClearFilter () {
    const { apiToken } = this.props
    this.setState({ filter: { q: '', ods: '' }, clearFilter: false })
    this._getInitiatives(apiToken, true)
  }

  renderSearchBtn () {
    if (this.state.clearFilter) {
      return (
        <Button
          transparent
          style={{marginVertical: 5, backgroundColor: '#ffcc00'}}
          onPress={() => {
            this.setState({loadingSearch: true})
            this.doClearFilter()
          }}>
          <Text style={{color: '#fff', fontWeight: 'bold'}}>Limpar</Text>
        </Button>
      )
    }
    return (
      <Button
        style={{marginVertical: 5, marginRight: 10, paddingHorizontal: 10, backgroundColor: '#52a34d'}}
        onPress={() => {
          this.doSearch()
        }}>
        <Text style={{ color: '#fff', fontWeight: 'bold' }}>Buscar</Text>
      </Button>
    )
  }

  _loadingSearch () {
    if (this.state.loadingSearch) {
      return (<Spinner width={25} style={{height: 25}} color='#666' />)
    }
  }
  // Só será usado depois quando tivermos os filtros
  // renderSearchBar () {
  //   if (this.state.showSearch) {
  //     return (
  //       <View >
  //         <Button transparent
  //           onPress={() => {
  //             this.doClearFilter()
  //             this.setState({showSearch: false, showSearchBtn: true})
  //           }}
  //           style={{alignSelf: 'flex-end', marginRight: 10}}
  //         >
  //           <Icon name='times' size={30} primary />
  //         </Button>

  //         <Item>
  //           <Input
  //             placeholder='Pesquisar...'
  //             value={this.state.filter.q}
  //             onChangeText={v => this.setFilterQ(v)} />

  //           <Picker
  //             mode='dialog'
  //             note={false}
  //             placeholder='Ods'
  //             style={{width: 100, height: 44}} itemStyle={{height: 44}}
  //             selectedValue={this.state.filter.ods}
  //             onValueChange={v => this.setFilterOds(v)}
  //             renderHeader={(backAction) =>
  //               <Header>
  //                 <Left>
  //                   <Button transparent onPress={backAction}>
  //                     <Icon name='times' primary />
  //                   </Button>
  //                 </Left>
  //                 <Body style={{ flex: 3 }}>
  //                   <Title>Filtrar por Ods</Title>
  //                 </Body>
  //                 <Right />
  //               </Header>
  //             }>
  //             {this.state.Ods.map((o, index) => <Picker.Item label={(o.number || '') + '- ' + o.title} value={o.id} key={index} />)}
  //           </Picker>
  //           {this.renderSearchBtn()}
  //         </Item>

  //       </View>
  //     )
  //   }
  // }
  // _renderSearchButton () {
  //   if (this.state.showSearchBtn) {
  //     return (
  //       <Button transparent
  //         onPress={() => { this.setState({showSearch: true, showSearchBtn: false}) }}
  //         style={{alignSelf: 'flex-end', marginRight: 10}}
  //       >
  //         <Image
  //           source={require('../../../Assets/Images/ico_search.png')}
  //           style={{height: 30, width: 30}}
  //         />
  //       </Button>
  //     )
  //   }
  // }

  // Temporário
  _searchBar () {
    return (
      <View >
        <Item>
          <Input
            placeholder='Pesquisar...'
            value={this.state.filter.q}
            onChangeText={v => this.setFilterQ(v)} />
          {this.renderSearchBtn()}
        </Item>
      </View>
    )
  }

  render () {
    if (this.state.showLoading) {
      return (<Spinner width={35} color='#666' />)
    }
    if (this.state.error) {
      return (<Text>Erro ao obter iniciativas: {this.state.errorText}</Text>)
    }

    return (
      <View style={{ flex: 1 }}>
        <AppHeader title='Iniciativas' navigation={this.props.navigation} />
        <View style={styles.container}>
          {/* this._renderSearchButton() */}
          {/* this.renderSearchBar() */}
          {this._searchBar()}
          {/* <Line /> */}
          {this._loadingSearch()}
          <List
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.refreshInitiatives.bind(this)}
                title='Atualizando...'
            />
          }
            dataArray={this.state.initiatives}
            renderRow={this.renderRow.bind(this)}
          />
          {/* <View style={{height: 20}}>
            {this.renderNextItemsSpinner()}
          </View> */}
        </View>
      </View>
    )
  }
}

export default connect(mapStateToProps, null)(InitiativesList)
