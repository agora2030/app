import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  listItem: {
    backgroundColor: 'transparent', 
    borderBottomWidth: 0.5, 
    borderBottomColor: 'rgba(0,0,0,.2)'
  },
  buttonLogout:{
    marginTop: 20,
    marginHorizontal: 50
  },
  buttonTextLogout:{
    color: '#fff'
  },  
})
