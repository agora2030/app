import React, { Component } from 'react'
import {
  ActionSheet, Container, Content, View, Thumbnail
} from 'native-base'
import { TouchableOpacity, Dimensions } from 'react-native'
import { connect } from 'react-redux'
import * as Progress from 'react-native-progress'
import Text from '../../../Components/text'
import H3 from '../../../Components/h3'
import AppHeader from '../../../Components/app-header'
import IcoText from '../../../Components/ico-text'
import ActivitiesList from './partials/activitieslist'
import styles from './styles'
import { getProfile, getUserActivities, getUserProfile, getProfileActivities } from '../../../Api/profile'
import { getMissionIcon, getUserAvatar, getUserNivelName,
  progressBarPercent, userNivelColor } from '../../../helpers'
import { updateUserProfile } from '../../../Redux/UserProfile/UserProfileActions'

var actionPhotoButtons = [
  'Câmera',
  'Galeria',
  'Cancelar'
]
var cancelIndex = 2

const { width, height } = Dimensions.get('window')
const mapStateToProps = state => (
  {
    apiToken: state.UserProfile.apiToken,
    firstName: state.UserProfile.firstName,
    socialAvatar: state.UserProfile.socialAvatar,
    occupation: state.UserProfile.occupation,
    state: state.UserProfile.state,
    city: state.UserProfile.city,
    fullProfile: state.UserProfile
  }
)

class Profile extends Component {
  constructor (props) {
    super(props)
    this.state = {
      selected1: 'key1',
      publicLoading: true,
      missionsLoading: true,
      activitiesLoading: true,
      activities: [],
      uid: null,
      viewProfile: {}
    }
  }

  onValueChange (value) {
    this.setState({
      selected1: value
    })
  }

  setPhoto () {
    ActionSheet.show(
      {
        options: actionPhotoButtons,
        cancelButtonIndex: cancelIndex
      },
      buttonIndex => {
      // this.setState({ clicked: BUTTONS[buttonIndex] });
      }
    )
  }

  componentWillMount () {
    const { apiToken, navigation } = this.props
    this.setState({ publicLoading: true, activitiesLoading: true })
    if (navigation.state.params && navigation.state.params.uid) {
      const { uid } = navigation.state.params
      this.setState({ activitiesLoading: false })
      getUserProfile('public', apiToken, uid)
        .then(
          success => {
            this.setState({ publicLoading: false, viewProfile: success.data, uid })
          },
          error => {
            this.setState({ publicLoading: false })
            console.log(error)
          }
        )
      getUserActivities(apiToken, '', uid)
        .then(
          success => {
            this.setState({ activitiesLoading: false, activities: success.data.items })
          },
          error => {
            this.setState({ activitiesLoading: false })
            console.log(error.response)
          }
        )
    } else {
      getProfile('public', apiToken)
        .then(
          success => {
            this.setState({ publicLoading: false })
            this.props.updateUserProfile(success.data)
          },
          error => {
            this.setState({ publicLoading: false })
            console.log(error)
          }
        )
      getProfileActivities(apiToken)
        .then(
          success => {
            console.log(success)
            this.setState({ activitiesLoading: false, activities: success.data.items })
          },
          error => {
            this.setState({ activitiesLoading: false })
            console.log(error.response)
          }
        )
    }
  }

  renderMissions () {
    let profile = this.props.fullProfile
    if (this.state.uid) {
      profile = this.state.viewProfile
    }
    if (profile.missions.length > 0) {
      return (
        <View style={[ styles.row, styles.stamps ]} padder>
          {
          profile.missions.map((item, index) => {
            return <IcoText source={getMissionIcon(item.title)} text={item.title} key={index} />
          })
        }
        </View>
      )
    } else {
      return (
        <View>
          <Text>Nenhuma missão concluída.</Text>
        </View>
      )
    }
  }

  renderProfileDetails () {
    let profile = this.props.fullProfile
    if (this.state.uid) {
      profile = this.state.viewProfile
    }
    const { occupation, city, state } = profile
    let completeTxt = null
    if (!this.state.uid) {
      completeTxt = (
        <TouchableOpacity onPress={() => this.props.navigation.navigate('Register', { isEdit: true })}>
          <Text style={{ color: 'blue', textDecorationLine: 'underline' }}>Complete seu perfil!</Text>
        </TouchableOpacity>
      )
    }
    if (!occupation && !city && !state) {
      return completeTxt
    }
    return (
      <View>
        {occupation ? (<Text style={styles.text}>{occupation}</Text>) : completeTxt}
        {(city || state) ? (<Text style={styles.text}>{city}, {state}</Text>) : completeTxt}
      </View>
    )
  }

  render () {
    let profile = this.props.fullProfile
    let showConfig = true
    if (this.state.uid) {
      profile = this.state.viewProfile
      showConfig = false
    }
    return (
      <Container style={{ backgroundColor: '#f3f3f3' }}>
        <AppHeader {...this.props} backButton title='Perfil' config={showConfig} />

        <Content style={{ marginTop: -25, position: 'absolute', paddingBottom: 150, top: 80, width: width, height: height, zIndex: -1 }}>
          <View>
            <View style={[styles.row, styles.header]} padder>
              <View style={styles.colThumbnail}>
                <Thumbnail
                  circle
                  large
                  source={getUserAvatar(profile)}
                  style={[styles.avatar, { borderColor: userNivelColor(profile.nivel) }]}
                />
              </View>

              <View style={styles.colInfo}>
                <H3 style={styles.nameUser}>{profile.firstName} {profile.lastName}</H3>
                {this.renderProfileDetails()}
                <Text style={styles.levelNane}>{getUserNivelName(profile.nivel)}</Text>

                <Progress.Bar
                  color={userNivelColor(profile.nivel)}
                  progress={progressBarPercent(profile)}
                  borderWidth={1}
                  borderRadius={10}
                  height={8}
                  width={160}
                />
              </View>
            </View>

            {this.renderMissions()}

            <View style={[ styles.row, styles.filter ]} padder>
              <View style={styles.colLabel}>
                <H3 text='Atividades'>Atividades Recentes</H3>
              </View>
              {/* <View style={ styles.colActivities }>
                <Item regular style={ styles.picker }>
                  <Picker
                    iosHeader="Filtrar Ação"
                    mode="dropdown"
                    selectedValue='key0'
                    >
                    <Picker.Item label="Visualizar" value="actionView" />
                    <Picker.Item label="Curtir" value="actionLike" />
                    <Picker.Item label="Favoritar" value="actionFavorite" />
                    <Picker.Item label="Comentar" value="actionComment" />
                    <Picker.Item label="Compartilhar" value="actionShare" />
                    <Picker.Item label="Apoiar" value="actionDonate" />
                  </Picker>
                </Item>
              </View> */}
            </View>

            <ActivitiesList
              isLoading={this.state.activitiesLoading}
              activities={this.state.activities}
            />
          </View>

        </Content>
      </Container>
    )
  }
}

export default connect(mapStateToProps, { updateUserProfile })(Profile)
