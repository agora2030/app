import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e6ebee'
  },
  row: {
    backgroundColor: '#e6ebee',
    borderBottomWidth: 0.5,
    borderBottomColor: 'rgba(0,0,0,.2)',
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row'
    // paddingVertical: 20,
  },
  colIco: {
    backgroundColor: '#35bb2f',
    borderTopRightRadius: 50,
    borderBottomRightRadius: 50,
    height: 50,
    padding: 20,
    marginRight: 20,
    justifyContent: 'center',
    // flex: 1,
    alignItems: 'flex-end'
  },
  colTitle: {
    justifyContent: 'center',
    marginRight: 20
  },
  title: {
    justifyContent: 'center',
    fontWeight: 'bold'
  },
  ico: {
    borderRadius: 25,
    height: 40,
    width: 40,
    marginTop: -5
  },
  innerText: {
    justifyContent: 'center',
    margin: 10,
    width: 170
  },
  viewNoCurators: {
    marginTop: 100, flex: 1, justifyContent: 'center', alignItems: 'center'
  },
  noCurators: {
    textAlign: 'center',
    marginBottom: 10
  },
  containerSpinner: {
    position: 'absolute',
    backgroundColor: 'rgba(255, 255, 255, .6)',
    bottom: 0,
    left: 0,
    right: 0,
    top: 80,
    flexDirection: 'column',
    justifyContent: 'center'
  }
})
