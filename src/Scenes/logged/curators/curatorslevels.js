import React, { Component } from 'react'
import {Dimensions, TouchableOpacity, View} from 'react-native'
import {Thumbnail} from 'native-base'
import AppHeader from '../../../Components/app-header'
import H3 from '../../../Components/h3'
import styles from './styles'

const { width, height } = Dimensions.get('window')

export default class CuratorsLevels extends Component {
  constructor (props) {
    super(props)
    this.state = {
      title: 'Curadores',
      size: { width, height }
    }
  }

  render () {
    const { navigate } = this.props.screenProps.rootNav

    return (
      <View style={styles.container}>
        <AppHeader {...this.props} title={this.state.title} />
        <TouchableOpacity style={styles.row} onPress={() => navigate('CuratorsLevel', {title: 'Nível Mundo', nivel: 5})}>
          <View style={[styles.colIco, { backgroundColor: '#35bb2f', width: 180 }]}>
            <Thumbnail
              small
              circle
              source={require('../../../Assets/Images/ico_curadores_nivel_mundo.png')}
              style={styles.ico}
                />
          </View>
          <View style={styles.colTitle}>
            <H3>Nível Mundo</H3>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.row} onPress={() => navigate('CuratorsLevel', {title: 'Nível País', nivel: 4})}>
          <View style={[styles.colIco, { backgroundColor: '#50d5b6', width: 160 }]}>
            <Thumbnail
              small
              circle
              source={require('../../../Assets/Images/ico_curadores_nivel_pais.png')}
              style={styles.ico}
              />
          </View>
          <View style={styles.colTitle}>
            <H3 style={styles.title}>Nível País</H3>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.row} onPress={() => navigate('CuratorsLevel', {title: 'Nível Comunidade', nivel: 3})}>
          <View style={[styles.colIco, { backgroundColor: '#f0c82a', width: 140 }]}>
            <Thumbnail
              small
              circle
              source={require('../../../Assets/Images/ico_curadores_nivel_comunidade.png')}
              style={styles.ico}
              />
          </View>
          <View style={styles.colTitle}>
            <H3 style={styles.title}>Nível Comunidade</H3>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.row} onPress={() => navigate('CuratorsLevel', {title: 'Nível Casa', nivel: 2})}>
          <View style={[styles.colIco, { backgroundColor: '#e6963d', width: 120 }]}>
            <Thumbnail
              small
              circle
              source={require('../../../Assets/Images/ico_curadores_nivel_casa.png')}
              style={styles.ico}
              />
          </View>
          <View style={styles.colTitle}>
            <H3 style={styles.title}>Nível Casa</H3>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.row} onPress={() => navigate('CuratorsLevel', {title: 'Nível Acampamento', nivel: 1})}>
          <View style={[styles.colIco, { backgroundColor: '#cba46c', width: 100 }]}>
            <Thumbnail
              small
              circle
              source={require('../../../Assets/Images/ico_curadores_nivel_acampamento.png')}
              style={styles.ico}
              />
          </View>
          <View style={styles.colTitle}>
            <H3 style={styles.title}>Nível Acampamento</H3>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}
