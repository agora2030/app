import React, { PureComponent } from 'react'
import { View, Image, DeviceEventEmitter, Dimensions } from 'react-native'
import { TabNavigator, NavigationActions } from 'react-navigation'
import SceneThree from '../carouselstart/scenethree'
import CuratorsLevels from '../logged/curators/curatorslevels'
import StartScene from '../start/start'
import InitiativesList from '../logged/initiativeslist/initiativeslist'

let activeTab = 0

const getActiveTabImg = (index, activeTab) => {
  const imgsTab = [
    {
      'default': require('../../Assets/Images/ico_tabs_home.png'),
      'active': require('../../Assets/Images/ico_tabs_home_active.png')
    },
    {
      'default': require('../../Assets/Images/ico_tabs_iniciativas.png'),
      'active': require('../../Assets/Images/ico_tabs_iniciativas_active.png')
    },
    {
      'default': require('../../Assets/Images/ico_tabs_curadores.png'),
      'active': require('../../Assets/Images/ico_tabs_curadores_active.png')
    },
    {
      'default': require('../../Assets/Images/ico_tabs_ajuda.png'),
      'active': require('../../Assets/Images/ico_tabs_ajuda_active.png')
    }
  ]
  if (index === activeTab) {
    return imgsTab[activeTab]['active']
  }
  return imgsTab[index]['default']
}

const { width } = Dimensions.get('window')

const TabsScreens = {
  Start: {
    screen: StartScene,
    navigationOptions: {
      tabBarLabel: 'Home',
      tabBarIcon: () => (<Image
        source={getActiveTabImg(0, activeTab)}
        style={{ height: 40, width: 85 }}
        />)
    }
  },
  Initiatives: {
    screen: InitiativesList,
    navigationOptions: {
      tabBarLabel: 'Iniciativas',
      tabBarIcon: () => (<Image
        source={getActiveTabImg(1, activeTab)}
        style={{height: 40, width: 95, zIndex: 1}}
        />)
    }
  },
  Curators: {
    screen: CuratorsLevels,
    navigationOptions: {
      tabBarLabel: 'Curadores',
      tabBarIcon: () => (<Image
        source={getActiveTabImg(2, activeTab)}
        style={{ height: 40, width: 85, zIndex: 1 }}
        />)
    }
  },
  Help: {
    screen: SceneThree,
    navigationOptions: {
      tabBarLabel: 'Ajuda',
      tabBarIcon: () => (<Image
        source={getActiveTabImg(3, activeTab)}
        style={{ height: 40, width: 85, zIndex: 2 }}
        />)
    }
  }
}

const Tabs = TabNavigator(TabsScreens, {
  tabBarPosition: 'bottom',
  animationEnabled: true,
  swipeEnabled: false,
  navigationOptions: ({ navigation }) => ({
    tabBarOnPress: (tab, jumpToIndex) => {
      activeTab = tab.index
      switch (tab.index) {
        case 0:
          DeviceEventEmitter.emit('updateTracestats')
          break
      }
      jumpToIndex(tab.index)
    }
  }),
  tabBarOptions: {
    showIcon: true,
    showLabel: false,
    style: {
      backgroundColor: '#fff'
    },
    indicatorStyle: {
      backgroundColor: '#509b4c',
      height: 50,
      zIndex: -1
    },
    iconStyle: {
      backgroundColor: 'transparent',
      height: 50,
      padding: 0,
      marginVertical: -12,
      width: Math.ceil(width / 4)

    },
    activeBackgroundColor: '#509b4c'
  }
})

export default class DashboardScene extends PureComponent {
  logout (expiredSession = true) {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: 'Login', params: { error401: expiredSession }})
      ]
    })
    this.props.navigation.dispatch(resetAction)
  }
  componentWillMount () {
    DeviceEventEmitter.addListener('doLogout', ({ expiredSession }) => this.logout(expiredSession))
  }
  componentDidMount () {
    console.info('monted', this.props)
  }

  render () {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <Tabs
          swipeEnabled={false}
          screenProps={{
            rootNav: this.props.navigation
          }}
        />
      </View>
    )
  }
}
