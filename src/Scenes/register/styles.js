import { Platform, StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  header: {
    resizeMode: 'cover',
    width: null,
    height: 200,
    marginBottom: 10
  },
  title: {
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center'
  },
  largeThumbnail: {
    width: 120,
    height: 120,
    borderRadius: 60,
    marginVertical: 15,
    alignSelf: 'center'
  },
  buttonDate: {
    backgroundColor: 'transparent',
    flex: 1
  },
  labelButtonDate: {
    color: '#575757',
    textAlign: 'left',
    marginLeft: 4
  },
  innerTextDate: {
    position: 'absolute',
    zIndex: -1,
    left: 120
  },
  lineList: {
    paddingVertical: 10
  },
  accept: {
    textAlign: 'center',
    marginHorizontal: 20,
    marginBottom: 10
  },
  closeModal: {
    alignSelf: 'flex-end',

    ...Platform.select({
      ios: {
        marginTop: -5,
        marginBottom: -5
      }
    })
  },
  photoSpinner: {
    backgroundColor: 'rgba(0,0,0,.5)',
    position: 'absolute',
    zIndex: 2,
    width: 120,
    height: 120,
    borderRadius: 60,
    marginVertical: 15,
    flexDirection: 'column',
    justifyContent: 'center',
    alignSelf: 'center'
  }
})
