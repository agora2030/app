import React, { Component } from 'react'
import {
  ActionSheet,
  Header,
  Title,
  Body,
  Left,
  Right,
  Form,
  Item as FormItem,
  Label,
  Input,
  Button,
  Thumbnail,
  Picker
} from 'native-base'
import {
  Image, StatusBar, ScrollView, TouchableOpacity, ActivityIndicator,
  View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker'
import ODS from '../../Components/ods'
import H2 from '../../Components/h2'
import H3 from '../../Components/h3'
import Text from '../../Components/text'
import mStyles from '../../Assets/Styles/Styles'
import styles from './styles'
import I18n from '../../Locales'
import {
  doRegister,
  doEditProfile,
  updateUserProfile
} from '../../Redux/UserProfile/UserProfileActions'
import Countries from '../../Assets/data/countries'
import Majors from '../../Assets/data/majors'
import StatesBr from '../../Assets/data/statesandcitiesfrombrazil'
import { setProfilePicture } from '../../Api/profile'
import { getUserAvatar, deleteEmptyKeys } from '../../helpers'
import AppHeader from '../../Components/app-header'

var actionPhotoButtons = [
  I18n.t('register.camera'),
  I18n.t('register.gallery'),
  I18n.t('register.cancel')
]
var cancelIndex = 2

const mapStateToProps = state => (
  {
    fullProfile: state.UserProfile
  }
)

const PickerItem = Picker.Item

const estadosCidades = {}

for (let i = 0; i < StatesBr.length; i++) {
  estadosCidades[StatesBr[i].code] = {
    name: StatesBr[i].name,
    cities: StatesBr[i].cities
  }
}

class Register extends Component {
  constructor (props) {
    super(props)
    let formBtnCtrl = true
    if (props.navigation.state.params && props.navigation.state.params.isEdit) {
      formBtnCtrl = !props.navigation.state.params.isEdit
    }
    if (props.fullProfile.socialLoginOrigins) {
      if (props.fullProfile.socialLoginOrigins.length > 0) {
        formBtnCtrl = false
      }
    }
    this.state = {
      modalDateVisible: false,
      avatarTemp: {},
      isUploadingAvatar: false,
      currentStateCities: 'selecione',
      disableFormBtn: formBtnCtrl,
      showDiferentPwdMsg: false,
      loadingCadastro: false,
      firstName: '',
      lastName: '',
      country: 'Brasil',
      state: props.fullProfile.state || 'selecione',
      city: '',
      socialAvatar: '',
      socialLoginOrigin: '',
      gender: '',
      major: '',
      occupation: '',
      phoneNumber: '',
      erroCadastro: '',
      pwdConfirm: '',
      password: ''
    }
  }
  componentDidMount () {
    if (this.props.navigation.state.params && this.props.navigation.state.params.isEdit) {
      const profile = deleteEmptyKeys(this.props.fullProfile)
      this.setState({ ...profile })
    }
  }

  comparePwds (p1, p2) {
    if (p1 !== p2) {
      this.setState({ disableFormBtn: true, showDiferentPwdMsg: true })
    } else {
      this.setState({ disableFormBtn: false, showDiferentPwdMsg: false })
    }
  }

  setPhoto () {
    ActionSheet.show(
      {
        options: actionPhotoButtons,
        cancelButtonIndex: cancelIndex
      },
      buttonIndex => {
        switch (buttonIndex) {
          case 0:
            this.getPicFromCamera()
            break
          case 1:
            this.getPicFromGallery()
            break
        }
      }
    )
  }

  getPicFromCamera () {
    const options = {
      cameraType: 'front',
      mediaType: 'photo'
    }
    ImagePicker.launchCamera(options, (response) => {
      // console.log('Response = ', response)

      if (response.didCancel) {
        console.log('User cancelled image picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton)
      } else {
        let source = { uri: response.uri }

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarTemp: source,
          isUploadingAvatar: true
        })

        setProfilePicture(this.props.fullProfile.apiToken, response)
          .then(
            success => {
              this.setState({ isUploadingAvatar: false })
              this.props.updateUserProfile(success.data)
              console.log(success)
            },
            error => {
              this.setState({ isUploadingAvatar: false })
              console.log(error)
            }
          )
      }
    })
  }

  getPicFromGallery () {
    const options = {
      mediaType: 'photo'
    }
    ImagePicker.launchImageLibrary(options, (response) => {
      // console.log('Response = ', response)

      if (response.didCancel) {
        console.log('User cancelled image picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton)
      } else {
        let source = { uri: response.uri }

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarTemp: source,
          isUploadingAvatar: true
        })

        setProfilePicture(this.props.fullProfile.apiToken, response)
          .then(
            success => {
              this.setState({ isUploadingAvatar: false })
              this.props.updateUserProfile(success.data)
              console.log(success)
            },
            error => {
              this.setState({ isUploadingAvatar: false })
              console.log(error)
            }
          )
      }
    })
  }

  renderAvatarLabel () {
    const { params } = this.props.navigation.state
    if (this.state.socialLoginOrigin.length === 0 && (params && params.isEdit)) {
      return (<Text style={[ mStyles.smallText, mStyles.smallTextCenter ]}>{ I18n.t('register.sendPhoto') }</Text>)
    }
  }

  renderAvatar () {
    const { params } = this.props.navigation.state
    if (this.props.fullProfile.socialAvatar || (params && params.isEdit)) {
      return (
        <TouchableOpacity onPress={() => {
          this.setPhoto()
        }}>
          {this.renderPhotoSpinner()}
          <Thumbnail
            style={styles.largeThumbnail}
            large
            source={getUserAvatar(this.props.fullProfile)}
          />
          {this.renderAvatarLabel()}
        </TouchableOpacity>
      )
    }
  }
  renderPhotoSpinner () {
    if (this.state.isUploadingAvatar) {
      return (<View style={styles.photoSpinner}>
        <ActivityIndicator />
      </View>)
    }
  }

  PwdsMsgs () {
    if (this.state.showDiferentPwdMsg) {
      return (<FormItem><Text>As senhas informadas não coincidem.</Text></FormItem>)
    }
  }
  // Bad name, bad name
  showInputsNotSocialAuth () {
    const { params } = this.props.navigation.state
    if (this.state.socialLoginOrigin.length === 0 && !(params && params.isEdit)) {
      return (
        <View>
          <FormItem rounded style={mStyles.input}>
            <Input
              placeholder='Senha'
              value={this.state.password}
              onChangeText={v => {
                this.setState({ password: v })
                this.comparePwds(v, this.state.pwdConfirm)
              }}
              secureTextEntry
            />
          </FormItem>
          <FormItem rounded style={mStyles.input}>
            <Input
              placeholder='Repita a senha'
              value={this.state.pwdConfirm}
              onChangeText={v => {
                this.setState({ pwdConfirm: v })
                this.comparePwds(this.state.password, v)
              }}
              secureTextEntry
            />
          </FormItem>
        </View>
      )
    }
  }

  _doRegister () {
    const {
      firstName,
      lastName,
      country,
      email,
      pwdConfirm,
      state,
      city,
      socialAvatar,
      socialLoginOrigin,
      gender,
      major,
      occupation,
      phoneNumber,
      password
    } = this.state

    const data = deleteEmptyKeys({ firstName,
      lastName,
      country,
      email,
      password,
      pwdConfirm,
      state,
      city,
      socialAvatar,
      socialLoginOrigin,
      gender,
      major,
      occupation,
      phoneNumber })
    const { params } = this.props.navigation.state
    let isValid = false

    if (data.socialLoginOrigin && data.socialLoginOrigin.length > 0) {
      delete data.password
      delete data.pwdConfirm
      if (
        data.firstName &&
        data.lastName
      ) {
        isValid = true
      }
    } else {
      delete data.socialLoginOrigin
      delete data.socialAvatar
      delete data.pwdConfirm
      if (
        data.firstName &&
        data.lastName &&
        data.email
      ) {
        isValid = true
      }
    }

    if (params && params.isEdit) {
      delete data.email
      delete data.socialLoginOrigin
      delete data.socialAvatar
      delete data.password
      if (
        data.firstName &&
        data.lastName
      ) {
        isValid = true
      }

      this.setState({ loadingCadastro: true })

      if (!isValid) {
        alert('Alguns campos são obrigatórios. Por favor revise os dados e tente novamente.')
      } else {
        // console.log('data a ser atualizado', data)
        this.props.doEditProfile(data)
          .then(
            success => {
              // console.log('success data', data)
              this.props.navigation.navigate('Dashboard')
              this.setState({ loadingCadastro: false })
            },
            error => {
              this.setState({ loadingCadastro: false })
            }
          )
      }
    } else {
      if (!isValid) {
        alert('Alguns campos são obrigatórios. Por favor revise os dados e tente novamente.')
      } else {
        this.props.doRegister(data)
          .then(
            success => {
              console.log('registro realizado com sucesso', success.data)
              this.props.updateUserProfile(success.data)
              this.setState({ loadingCadastro: true, password: '' })
              this.props.navigation.navigate('Login')
            },
            error => {
              this.setState({ loadingCadastro: false })
            }
          )
      }
    }
  }

  renderRegisterBtn () {
    const { params } = this.props.navigation.state
    if (this.state.loadingCadastro) {
      return (<ActivityIndicator color='#fff' />)
    }
    if (params && params.isEdit) {
      return (<Text style={{ color: '#fff' }}>Salvar</Text>)
    }
    return (<Text style={{ color: '#fff' }}>{ I18n.t('register.confirmRegister') }</Text>)
  }

  renderHeaderTitles () {
    const { params } = this.props.navigation.state
    if (!(params && params.isEdit)) {
      return (<View>
        <H2 style={styles.title}>{ I18n.t('register.register') }</H2>
        <Text style={mStyles.smallTextCenter}>{ I18n.t('register.intro') }</Text>
      </View>)
    }
  }

  renderStateAndcityFields () {
    if (this.state.country === 'Brasil') {
      return (<View>
        <FormItem rounded style={mStyles.input}>
          <Label>{ I18n.t('register.state') }</Label>
          <Picker
            mode='dialog'
            note={false}
            selectedValue={this.state.state}
            onValueChange={(v, index) => {
              this.setState({
                currentStateCities: v,
                state: v
              })
            }}
            renderHeader={backAction =>
              <Header>
                <Left>
                  <Button transparent onPress={backAction}>
                    <Icon name='times' primary />
                  </Button>
                </Left>
                <Body style={{ flex: 3 }}>
                  <Title>{ I18n.t('register.state') }</Title>
                </Body>
                <Right />
              </Header>
            }
            style={{ width: 230 }}
            >
            { StatesBr.map((state, index) => <PickerItem label={state.name} value={state.code} key={index} />) }
          </Picker>
        </FormItem>

        <FormItem rounded style={mStyles.input}>
          <Label>{ I18n.t('register.city') }</Label>
          <Picker
            mode='dialog'
            note={false}
            selectedValue={this.state.city}
            onValueChange={v => this.setState({ city: v })}
            disabled={this.state.state === 'select' || this.state.state === ''}
            renderHeader={backAction =>
              <Header>
                <Left>
                  <Button transparent onPress={backAction}>
                    <Icon name='times' primary />
                  </Button>
                </Left>
                <Body style={{ flex: 3 }}>
                  <Title>{ I18n.t('register.city') }</Title>
                </Body>
                <Right />
              </Header>
            }
            style={{ width: 230 }}
            >
            {estadosCidades[this.state.state].cities.map((city, index) => <PickerItem label={city} value={city} key={index} />) }
          </Picker>
        </FormItem>

      </View>)
    }
    return (<View>

      <FormItem rounded style={mStyles.input}>
        <Input
          placeholder={I18n.t('register.state')}
          value={this.state.state}
          onChangeText={v => this.setState({ state: v })}
        />
      </FormItem>

      <FormItem rounded style={mStyles.input}>
        <Input
          placeholder={I18n.t('register.city')}
          value={this.state.city}
          onChangeText={v => this.setState({ city: v })}
        />
      </FormItem>

    </View>)
  }

  renderTerms () {
    const { params } = this.props.navigation.state
    if (!(params && params.isEdit)) {
      return (<Text
        style={[ mStyles.smallText, styles.accept ]}
        >
        { I18n.t('register.acceptterms') }
      </Text>)
    }
  }

  renderScreenHeader () {
    const { params } = this.props.navigation.state
    if (params && params.isEdit) {
      return (<AppHeader backButton title='Editar Perfil' navigation={this.props.navigation} />)
    }
    return (<Image
      source={require('../../Assets/Images/header.png')}
      style={styles.header}
      />)
  }

  renderEditProfileFields () {
    const { params } = this.props.navigation.state
    if (params && params.isEdit) {
      return (
        <View>
          <FormItem rounded style={mStyles.input}>
            <Label>{ I18n.t('register.phone') }</Label>
            <Input
              refInput={ref => { this.input = ref }}
              placeholder={'+99 (099) 9999 999 999'}
              value={this.state.phoneNumber}
              style={mStyles.TextInputMask}
              underlineColorAndroid='transparent'
              onChangeText={v => this.setState({ phoneNumber: v })}
              keyboardType={'numeric'}
            />
          </FormItem>

          <View>
            <H3 style={mStyles.h3}>{ I18n.t('register.localization') }</H3>
            <FormItem rounded style={mStyles.input}>
              <Label>{ I18n.t('register.country') }</Label>
              <Picker
                mode='dialog'
                note={false}
                selectedValue={this.state.country}
                onValueChange={v => this.setState({ country: v })}
                renderHeader={backAction =>
                  <Header>
                    <Left>
                      <Button transparent onPress={backAction}>
                        <Icon name='times' primary />
                      </Button>
                    </Left>
                    <Body style={{ flex: 3 }}>
                      <Title>{ I18n.t('register.country') }</Title>
                    </Body>
                    <Right />
                  </Header>
                }
                style={{ width: 230 }}
                >
                { Countries.map((country, index) => <PickerItem label={country} value={country} key={index} />) }
              </Picker>
            </FormItem>

            {this.renderStateAndcityFields()}

          </View>
          <View>
            <H3 style={mStyles.h3}>Informações adicionais</H3>
            <FormItem rounded style={mStyles.input}>
              <Label>{ I18n.t('register.graduation')}</Label>
              <Picker
                mode='dropdown'
                note='teste'
                selectedValue={this.state.major}
                onValueChange={(v) => this.setState({ major: v })}
                style={{ width: 230 }}
                renderHeader={backAction =>
                  <Header>
                    <Left>
                      <Button transparent onPress={backAction}>
                        <Icon name='times' primary />
                      </Button>
                    </Left>
                    <Body style={{ flex: 3 }}>
                      <Title>{ I18n.t('register.graduation') }</Title>
                    </Body>
                    <Right />
                  </Header>
                  }

                >
                {Majors.map((major, index) => <PickerItem label={major} value={major} key={index} />)}
              </Picker>
            </FormItem>
            <FormItem rounded style={mStyles.input}>
              <Label>{ I18n.t('register.occupation')}</Label>
              <Input
                value={this.state.occupation}
                onChangeText={v => this.setState({ occupation: v })}
              />
            </FormItem>

            <FormItem rounded style={mStyles.input}>
              <Label>{ I18n.t('register.gender')}</Label>
              <Picker
                mode='dropdown'
                placeholder={I18n.t('register.gender')}
                selectedValue={this.state.gender}
                onValueChange={(v) => this.setState({ gender: v })}
                renderHeader={backAction =>
                  <Header>
                    <Left>
                      <Button transparent onPress={backAction}>
                        <Icon name='times' primary />
                      </Button>
                    </Left>
                    <Body style={{ flex: 3 }}>
                      <Title>{ I18n.t('register.gender')}</Title>
                    </Body>
                    <Right />
                  </Header>
                }
                style={{ width: 230 }}
                >
                <Picker.Item label='Selecione' value='' />
                <Picker.Item label='Feminino' value='Feminino' />
                <Picker.Item label='Masculino' value='Masculino' />
              </Picker>

            </FormItem>

          </View>
        </View>
      )
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <StatusBar hidden />

        <ScrollView>
          {this.renderScreenHeader()}

          <View padder>
            {this.renderHeaderTitles()}

            { this.renderAvatar() }
          </View>

          <Form style={{ backgroundColor: '#fff', flex: 1, margin: 0 }}>
            <View>
              <View style={{ paddingRight: 10, marginLeft: -5 }}>
              <FormItem regular inlineLabel style={mStyles.input}>
                <Label style={mStyles.label}>{ I18n.t('register.name') }</Label>
                <Input
                  value={this.state.firstName}
                  onChangeText={v => this.setState({ firstName: v })}
                />
              </FormItem>
              <FormItem regular inlineLabel style={mStyles.input}>
                <Label style={mStyles.label}>{ I18n.t('register.lastName') }</Label>
                <Input
                  value={this.state.lastName}
                  onChangeText={v => this.setState({ lastName: v })}
                />
              </FormItem>
              </View>

              <View style={{ paddingHorizontal: 10 }}>
                { this.showInputsNotSocialAuth() }

                {this.PwdsMsgs()}

                <FormItem rounded style={mStyles.input}>
                  <Label>{ I18n.t('register.email') }</Label>
                  <Input
                    placeholder={'your@email.com'}
                    value={this.state.email}
                    keyboardType={'email-address'}
                    editable={
                      this.state.socialLoginOrigin.length > 0 ? false : true ||
                      (this.props.navigation.state.params && this.props.navigation.state.params.isEdit)
                    }
                    onChangeText={v => this.setState({ email: v })}
                    />
                </FormItem>

                {this.renderEditProfileFields()}
              </View>

              <View style={{ margin: 15, paddingHorizontal: 10 }}>
                {this.renderTerms()}

                <Button
                  small
                  block
                  primary
                  rounded
                  style={mStyles.buttonPrimary}
                  onPress={() => this._doRegister()}
                  disabled={this.state.loadingCadastro || this.state.disableFormBtn}
                >
                  { this.renderRegisterBtn() }
                </Button>

              </View>
              <ODS />
            </View>
          </Form>
        </ScrollView>
      </View>
    )
  }
}

export default connect(
  mapStateToProps,
  {
    doRegister,
    doEditProfile,
    updateUserProfile
  }
)(Register)
