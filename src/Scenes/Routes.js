import { StackNavigator } from 'react-navigation'

import LoginScene from './login/login'
import DashboardScene from './dashboard/dashboard'
import StartScene from './start/start'
import RegisterScene from './register/register'
import ProfileScene from './logged/profile/profile'
import CommentsScene from './logged/comments/comments'
import TutorialScene from './carouselstart/carouselstart'
import InitialScene from './initial/initial'
import InitiativesListScene from './logged/initiativeslist/initiativeslist'
import UserInitiativesListScene from './logged/initiativeslist/userinitiativeslist'
import InitiativeViewScene from './logged/initiativeview/iniativeview'
import CuratorsLevels from './logged/curators/curatorslevels'
import CuratorsLevel from './logged/curators/curatorslevel'
import Config from './logged/config/config'
import LoginSOcial from './login/loginsocial'
import Mission from './mission/mission'
import RecoverPwd from './recoverpwd/recoverpwd'
import PhotoZoomer from './photozoomer/photozoomer'

const Routes = {
  Initial: { screen: InitialScene },
  Tutorial: { screen: TutorialScene },
  Login: { screen: LoginScene },
  LoginSocial: { screen: LoginSOcial },
  Dashboard: { screen: DashboardScene },
  Start: { screen: StartScene },
  Register: { screen: RegisterScene },
  Profile: { screen: ProfileScene },
  Comments: { screen: CommentsScene },
  InitiativesList: { screen: InitiativesListScene },
  UserInitiativesList: { screen: UserInitiativesListScene },
  InitiativeView: { screen: InitiativeViewScene },
  CuratorsLevels: { screen: CuratorsLevels },
  CuratorsLevel: { screen: CuratorsLevel },
  Config: { screen: Config },
  Mission: { screen: Mission },
  RecoverPwd: { screen: RecoverPwd },
  PhotoZoomer: { screen: PhotoZoomer }
}

const RootNavigator = StackNavigator(
  Routes,
  {
    headerMode: 'none',
    initialRouteName: 'Initial'
  }
)

export default RootNavigator
