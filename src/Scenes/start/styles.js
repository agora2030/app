import { Platform, StyleSheet, Dimensions } from 'react-native'
const { height } = Dimensions.get('window')
let marginTop = (height <= 960) ? -20 : -4

export default StyleSheet.create({
  container: {
    position: 'relative',
    marginTop: 180,
    zIndex: 10,
    flex: 1
  },
  mainView: {
    backgroundColor: 'transparent',
    flex: 1,
    margin: 0
  },
  backgroundImage: {
    backgroundColor: 'transparent',
    position: 'absolute',
    zIndex: 1,
    ...Platform.select({
      ios: {
        left: 0,
        top: 0
      },
      android: {
        top: -10,
        left: 0,
        right: 0
      }
    })
  },
  userHeader: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 1,
    height: 175
  },
  boxUserInfo: {
    ...Platform.select({
      ios: {
        position: 'relative',
        zIndex: -1
      },
      android: {
      }
    })
  },
  row: {
    flexDirection: 'row'
  },
  rowStamps: {
    flexDirection: 'row',
    justifyContent: 'center',
    ...Platform.select({
      ios: {
        marginTop: marginTop
      }
    })
  }
})
