import { Platform, StyleSheet } from 'react-native';

export default StyleSheet.create({
  backgroundImage: { 
    backgroundColor: '#c9cbd0',
    flex: 1, 
    height: undefined, 
    width: undefined ,
    resizeMode: 'cover' 
  },
  boxAction: {
    height: 260, 
    alignItems: 'center', 
    justifyContent: 'center', 
    marginHorizontal: 30 
  },
  modal: {
    ...Platform.select({
      ios: {
        margin: 20
      },
      android: {
        margin: 0
      }
    })
  },
  boxFioInfo: {
    position: 'absolute',
    alignItems: 'center',
    bottom: 10
  },
  boxLogin: {
    backgroundColor: '#4ca047',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 20,
    paddingHorizontal: 30,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,

    ...Platform.select({
      ios: {
        margin: -20
      },
      android: {
        margin: 0
      }
    }) 
  }, 
  row: { 
    flexDirection: 'row',
    justifyContent: 'center'
  },
  introText: { 
    color: '#fff', 
    textAlign: 'center', 
    marginBottom: 10 
  },
  item: {
    borderColor: 'transparent',
    backgroundColor: '#fff',
    marginBottom: 10
  },
  noteAction: { 
    color: '#fff', 
    fontSize: 12, 
    textAlign: 'center', 
    marginBottom: 14
  },
  title: {
    textAlign: 'center',
    marginBottom: 20
  }
});