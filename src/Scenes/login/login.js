import React, { Component } from 'react'
import { Platform, Image, TouchableOpacity, AsyncStorage,
  DeviceEventEmitter, View, ActivityIndicator } from 'react-native'
import {
  Item,
  Input,
  Button,
  Toast
} from 'native-base'
import OneSignal from 'react-native-onesignal'
import { connect } from 'react-redux'
import Modal from 'react-native-modal'
import Text from '../../Components/text'
import H3 from '../../Components/h3'
import {
  logIn
} from '../../Api/auth'

import mStyles from '../../Assets/Styles/Styles'
import styles from './styles'

import {
  updateUserProfile
} from '../../Redux/UserProfile/UserProfileActions'

class Login extends Component {
  state = {
    email: '',
    password: '',
    loadingLogin: false,
    loadingCadastro: false,
    loadingSocial: false,
    showModalLogin: false
  }
  componentDidMount () {
    const { params } = this.props.navigation.state
    if (params && params.error401) {
      this.props.updateUserProfile('logout')
      Toast.show({
        text: 'Sua sessão expirou. Por favor, faça login novamente',
        position: 'bottom',
        type: 'warning',
        buttonText: '×',
        duration: 6000
      })
    }
  }
  renderLoginBtn () {
    if (this.state.loadingLogin) {
      return (<ActivityIndicator color='#4c9c47' />)
    }
    return (<Text style={{ color: '#4c9c47' }}>Entrar</Text>)
  }

  _doLogin () {
    const { email, password } = this.state
    this.setState({ loadingLogin: true })
      logIn({ email, password })
        .then(
          success => {
            this.setState({ loadingLogin: false, password: '', showModalLogin: false })
            AsyncStorage.setItem('apiToken', success.data.token)
            OneSignal.sendTag('id', success.data.id)
            DeviceEventEmitter.emit('initPersistence', { source: 'login.js' })
            this.props.updateUserProfile(success.data)
            AsyncStorage.getItem('showTutorial', (err, value) => {
              if (err) {
                console.log('erro', err)
              }
              if (value && value === 'false') {
                this.props.navigation.navigate('Dashboard')
              } else {
                this.props.navigation.navigate('Tutorial')
              }
            })
          },
          error => {
            this.setState({ loadingLogin: false, password: '', showModalLogin: false })
            console.log(error.response)
            Toast.show({
              text: error.response.data.message,
              position: 'bottom',
              duration: 3000
            })
          }
        )

    // this._hideModal();
  }

  _modalAvoidKeyboard () {
    return (Platform.OS === 'ios')
  }

  goToRegister () {
    this.setState({ showModalLogin: false })
    this.props.updateUserProfile('logout')
    this.props.navigation.navigate('Register')
  }

  goToRecoverPwd () {
    this.setState({ showModalLogin: false })
    this.props.navigation.navigate('RecoverPwd')
  }

  render () {
    const { navigate } = this.props.navigation
    const { showModalLogin, email, password, loadingLogin, loadingSocial } = this.state
    return (
      <View style={{flex: 1}}>
        <Image
          source={require('../../Assets/Images/bg_login.jpg')}
          style={styles.backgroundImage}
        >
          <View style={{ backgroundColor: 'transparent', flex: 1, justifyContent: 'flex-end' }}>
            <View style={styles.boxAction}>
              <H3 style={[ styles.introText, { color: '#424649', position: 'absolute', top: 28 } ]}>Curadoria de inovações para um mundo sustentável</H3>
              <Button
                rounded
                primary
                block
                onPress={() => this.setState({ showModalLogin: true })}
                style={{ backgroundColor: '#4ca047', marginBottom: 20 }}
                >
                <Text style={[mStyles.textButtonPrimary, {paddingTop: 8}]}>Começar</Text>
              </Button>

              <View style={styles.boxFioInfo}>
                <H3>Fundação Oswaldo Cruz</H3>
                <Text style={{ marginVertical: 10 }}>Desafio Àgora 2030</Text>
                <Text note>Versão 1.0.0</Text>
              </View>
            </View>
          </View>
        </Image>

        <Modal
          animationIn='slideInUp'
          isVisible={showModalLogin}
          onBackdropPress={() => this.setState({ showModalLogin: false })}
          avoidKeyboard={this._modalAvoidKeyboard()}
          style={styles.modal}
          >
          <View style={styles.boxLogin}>
            <Text style={styles.introText}>Faça seu login através das redes sociais</Text>
            <View style={styles.row}>
              <TouchableOpacity onPress={() => {
                // this.props.modificaModalLogin()
                this.setState({ loadingSocial: true, showModalLogin: false })
                this.props.updateUserProfile('logout')
                navigate('LoginSocial', { type: 'facebook' })
              }}>
                <Image
                  style={{ width: 60, height: 60 }}
                  source={require('../../Assets/Images/ico_face.png')}
                  />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => {
                // this.props.modificaModalLogin()
                this.setState({ loadingSocial: true, showModalLogin: false })
                this.props.updateUserProfile('logout')
                navigate('LoginSocial', { type: 'google' })
              }}>
                <Image
                  style={{ width: 60, height: 60 }}
                  source={require('../../Assets/Images/ico_google.png')}
                  />
              </TouchableOpacity>
            </View>

            <Text style={styles.introText}>ou através do seu e-mail</Text>
            <Item rounded style={styles.item}>
              <Input
                placeholder='E-mail'
                placeholderTextColor='rgba(0,0,0, 0.4)'
                value={email}
                keyboardType={'email-address'}
                onChangeText={v => this.setState({ email: v })}
              />
            </Item>

            <Item rounded style={styles.item}>
              <Input
                placeholder='Senha'
                type='passsword'
                placeholderTextColor='rgba(0,0,0, 0.4)'
                secureTextEntry
                value={password}
                onChangeText={v => this.setState({ password: v })}
              />
            </Item>

            <Button
              rounded
              block
              disabled={loadingLogin}
              onPress={() => this._doLogin()}
              style={{ backgroundColor: '#baff00', marginHorizontal: 50 }}
              >
              {this.renderLoginBtn()}
            </Button>

            <View style={{ marginTop: 20 }}>
              <TouchableOpacity onPress={() => { this.goToRegister() }}>
                <Text style={styles.noteAction}>
                  Não tem uma conta? Cadastre-se
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.goToRecoverPwd()}>
                <Text style={styles.noteAction}>
                  Esqueceu a senha? Clique aqui
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

      </View>
    )
  }
}

export default connect(
  null,
  {
    updateUserProfile
  }
)(Login)
