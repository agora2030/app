import React, { Component } from 'react'
import { AsyncStorage, Alert, DeviceEventEmitter,
  ActivityIndicator, View, Text } from 'react-native'
import { connect } from 'react-redux'
import {
  googleRegisterOrLogin,
  facebookRegisterOrLogin,
  updateUserProfile
} from '../../Redux/UserProfile/UserProfileActions'

class LoginSocial extends Component {
  componentDidMount () {
    const { state } = this.props.navigation
    this.Redirect()
    if (state.params && state.params.type === 'google') {
      setTimeout(() => this.googleAuth(), 500)
    }
    if (state.params && state.params.type === 'facebook') {
      setTimeout(() => this.fbAuth(), 500)
    }
  }
  Redirect () {
    const { navigate } = this.props.navigation
    AsyncStorage.multiGet(['apiToken', 'showTutorial'], (err, data) => {
      if (err) {
        console.log('erro', err)
      }
      let loginExpired = false
      let route = 0
      let routes = [false, 'Tutorial', 'Dashboard']

      data.map(item => {
        const key = item[0]
        const value = item[1]
        if (key === 'apiToken' && value && value.length > 0) {
          route = 1
          loginExpired = true
        }
        if (key === 'showTutorial' && value && value === 'false' && loginExpired) {
          route = 2
        }
      })
      if (routes[route]) {
        navigate(routes[route])
      }
    })
  }
  fbAuth () {
    this.props.facebookRegisterOrLogin()
      .then(
        success => {
          this.props.updateUserProfile(success.data.data)
          DeviceEventEmitter.emit('initPersistence', { source: 'loginsocial.js' })
          AsyncStorage.getItem('showTutorial', (err, value) => {
            if (err) {
              console.log('Erro ao pegar tutorial do storage: ', err)
            }
            if (value && value === 'false') {
              this.props.navigation.navigate('Dashboard')
            } else {
              this.props.navigation.navigate('Tutorial')
            }
          })
        },
        error => {
          console.log(error)
          this.props.navigation.navigate('Login')
          Alert.alert(
            'Erro',
            'Não foi possível efetuar login com o Facebook. Tente novamente mais tarde, ou utilize outra forma de login',
            [
              {
                text: 'Ok',
                onPress: () => {
                  // this.props.navigation.navigate('Login')
                }
              }
            ]
          )
        }
      )
  }

  googleAuth () {
    this.props.googleRegisterOrLogin()
      .then(
        success => {
          DeviceEventEmitter.emit('initPersistence', { source: 'loginsocial.js' })
          this.props.updateUserProfile(success.data.data)
          AsyncStorage.getItem('showTutorial', (err, value) => {
            if (err) {
              console.log('Erro ao pegar tutorial do storage: ', err)
            }
            if (value && value === 'false') {
              this.props.navigation.navigate('Dashboard')
            } else {
              this.props.navigation.navigate('Tutorial')
            }
          })
        },
        error => {
          console.log(error)
          this.props.navigation.navigate('Login')
          Alert.alert(
            'Erro',
            'Não foi possível efetuar login com o Google. Tente novamente mais tarde, ou utilize outra forma de login',
            [
              {
                text: 'Ok',
                onPress: () => {
                  // this.props.navigation.navigate('Login')
                }
              }
            ]
          )
        }
      )
  }
  render () {
    return (
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator />
        <Text>
          Aguarde...
        </Text>
      </View>
    )
  }
}

export default connect(null, {
  googleRegisterOrLogin,
  facebookRegisterOrLogin,
  updateUserProfile
})(LoginSocial)
