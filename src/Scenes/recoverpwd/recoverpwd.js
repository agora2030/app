import React, { Component } from 'react'
import { Button, Input } from 'native-base'
import { Alert, View, ScrollView, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'

import { recoverPassword } from '../../Api/profile'

import Text from '../../Components/text'
import AppHeader from '../../Components/app-header'

const mapStateToProps = state => ({
  email: state.UserProfile.email
})

class RecoverPwd extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isLoading: false,
      errorMsg: '',
      email: props.email || ''
    }
  }
  _recoverPwd () {
    this.setState({ isLoading: true, errorMsg: '' })
    recoverPassword(this.state.email)
      .then(
        success => {
          this.setState({ isLoading: false })
          Alert.alert(
            'Email enviado',
            'Você deve ter recebido um email com instruções para a troca de senha.',
            [{
              text: 'Ok',
              onPress: () => {
                this.props.navigation.goBack()
              }
            }]
          )
        },
        error => {
          console.log('error', error.response)
          this.setState({ isLoading: false, errorMsg: error.response.data.message })
        }
      )
  }
  renderForm () {
    const { params } = this.props.navigation.state
    if (this.state.isLoading) {
      return (<ActivityIndicator />)
    }
    if (params && params.isReset) {
      return (<Button
        full
        primary
        disabled={this.state.email.length === 0}
        onPress={() => this._recoverPwd()}
      >
        <Text style={{ color: '#fff' }}>Enviar email</Text>
      </Button>)
    }
    return (<View>
      <Input
        placeholder='E-mail'
        placeholderTextColor='rgba(0,0,0, 0.4)'
        value={this.state.email}
        keyboardType={'email-address'}
        onChangeText={v => this.setState({ email: v })}
      />
      <Button
        full
        primary
        disabled={!this.state.email}
        onPress={() => this._recoverPwd()}
      >
        <Text style={{ color: '#fff' }}>Enviar email</Text>
      </Button>
    </View>)
  }
  renderText () {
    const { params } = this.props.navigation.state
    if (params && params.isReset) {
      return (<Text>
        Clique no botão abaixo para iniciar o processo de troca de senha.
        </Text>)
    }
    return (<Text>
      Caso deseje recuperar sua senha, informe seu email e clique no botão abaixo.
      </Text>)
  }
  render () {
    // const { navigate, goBack } = this.props.navigation
    return (
      <View>

        <AppHeader {...this.props} title='Recuperar senha' backButton />

        <ScrollView style={{padding: 10}}>
          {this.renderText()}
          {this.renderForm()}
          <Text style={{ marginVertical: 20, color: '#f00', textAlign: 'center' }}>{this.state.errorMsg}</Text>
        </ScrollView>

      </View>
    )
  }
}

export default connect(mapStateToProps, { })(RecoverPwd)
