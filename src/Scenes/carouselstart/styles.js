import { Platform, StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    backgroundColor: '#e6ebee',
    flex: 1,
    paddingBottom: 23
  },
  header: {
    // resizeMode: 'center',
    // width: null,
    // height: 170,
    // margin: -10,
    // marginBottom: 10
    width: null,
    resizeMode: 'contain',
    marginVertical: -15
  },
  fullImage: {
    width: null,
    resizeMode: 'contain',
    marginVertical: -15
  },
  title: {
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
    lineHeight: 38
  },
  intro: {
    textAlign: 'center',
    marginVertical: 10,
    marginHorizontal: 20
  },
  text: {
    textAlign: 'center',
    lineHeight: 20,
    marginHorizontal: 20
  },
  p: {
    textAlign: 'center',
    marginBottom: 10
  },
  rowIcos: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginHorizontal: 20
  },
  ico: {
    width: 60,
    height: 60,
    alignSelf: 'center'
  },
  icoText: {
    color: '#3d4148',
    textAlign: 'center',
    fontSize: 13,
    marginBottom: 15
  },
  smallModalContainer: {
    alignSelf: 'center',
    margin: 30,
    width: 270
  },
  smallModalContent: {
    backgroundColor: '#fff',
    borderRadius: 15,
    padding: 20
  },
  smallModalClose: {
    color: 'rgba(0,0,0,.5)',
    marginTop: -25,
    ...Platform.select({
      android: {
        marginTop: -17
      }
    })
  },
  smallModalImage: {
    width: 100,
    height: 100,
    marginTop: -25,
    marginBottom: 5,
    alignSelf: 'center'
  },
  smallModalTitle: {
    textAlign: 'center',
    fontWeight: 'bold'
  },
  smallModalLine: {
    backgroundColor: 'rgba(0,0,0,.2)',
    height: 0.5,
    marginBottom: 10,
    marginHorizontal: 20
  }
})
