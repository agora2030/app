import React, { Component } from 'react'
import { Container, Content, Button } from 'native-base'
import { View, ScrollView, Image, TouchableOpacity } from 'react-native'
import styles from './styles'
import I18n from '../../Locales'
import Modal from 'react-native-modal'
import Icon from 'react-native-vector-icons/FontAwesome'
import Text from '../../Components/text'
import H3 from '../../Components/h3'
import {faqUrl} from '../../Api/api.variables'
// const { width, height } = Dimensions.get('window')
import Communications from 'react-native-communications'
import mStyles from '../../Assets/Styles/Styles'

export default class SceneThree extends Component {
  constructor (props) {
    super(props)
    this.state = {
      modalFavorite: false,
      modalLike: false,
      modalDonate: false,
      modalView: false,
      modalShare: false,
      modalComment: false
    }
  }

  _ModalVisible (opts) {
    this.setState(opts)
  }

  render () {
    return (
      <View style={[styles.container, this.props.customStyle, { paddingHorizontal: 10 }]}>
        <ScrollView>
          <Image
            source={require('../../Assets/Images/tutorial_screen_02_header.png')}
            style={[ styles.header, {height: 170} ]}
            />

          <View padder>
            <H3 style={[styles.title, { marginTop: 10 }]}>{ I18n.t('tutorial.three.title') }</H3>
            <Text style={styles.p}>{ I18n.t('tutorial.three.text') }</Text>
            <Text style={{textAlign: 'center', fontSize: 11 }}>{ I18n.t('tutorial.three.text1') }</Text>

            <View style={[styles.rowIcos, { marginTop: 5 }]}>
              <TouchableOpacity onPress={() => { this.setState({ modalView: true }) }}>
                <Image
                  source={require('../../Assets/Images/actions/actionView.png')}
                  style={styles.ico} />
                <Text style={styles.icoText}>Visualizar</Text>
                <Modal
                  isVisible={this.state.modalView}
                  onBackdropPress={() => { this._ModalVisible({modalView: false}) }}
                  style={styles.smallModalContainer}
                >
                  <View style={styles.smallModalContent}>
                    <Button onPress={() => { this._ModalVisible({modalView: false}) }}
                      transparent small style={mStyles.modalBtClose}>
                      <Icon name='times' size={20} style={mStyles.modalIcoClose} />
                    </Button>
                    <Image
                      source={require('../../Assets/Images/actions/actionView.png')}
                      style={styles.smallModalImage} />
                    <Text style={styles.smallModalTitle}>Visualizar (Ler)</Text>
                    <View style={styles.smallModalLine} />
                    <Text style={styles.p}>{I18n.t('tutorial.three.view')}</Text>
                  </View>
                </Modal>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => { this.setState({ modalFavorite: true }) }}>
                <Image
                  source={require('../../Assets/Images/actions/actionFavorite.png')}
                  style={styles.ico} />
                <Text style={styles.icoText}>Favoritar</Text>
                <Modal
                  isVisible={this.state.modalFavorite}
                  onBackdropPress={() => { this._ModalVisible({modalFavorite: false}) }}
                  style={styles.smallModalContainer}
                >
                  <View style={styles.smallModalContent}>
                    <Button onPress={() => { this._ModalVisible({modalFavorite: false}) }}
                      transparent small style={mStyles.modalBtClose}>
                      <Icon name='times' size={20} style={mStyles.modalIcoClose} />
                    </Button>
                    <Image
                      source={require('../../Assets/Images/actions/actionFavorite.png')}
                      style={styles.smallModalImage} />
                    <Text style={styles.smallModalTitle}>Favoritar</Text>
                    <View style={styles.smallModalLine} />
                    <Text style={styles.p}>{I18n.t('tutorial.three.favorite')}</Text>
                  </View>
                </Modal>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => { this.setState({ modalComment: true }) }}>
                <Image
                  source={require('../../Assets/Images/actions/actionComment.png')}
                  style={styles.ico} />
                <Text style={styles.icoText}>Comentar</Text>
                <Modal
                  isVisible={this.state.modalComment}
                  onBackdropPress={() => { this._ModalVisible({modalComment: false}) }}
                  style={styles.smallModalContainer}
                >
                  <View style={styles.smallModalContent}>
                    <Button onPress={() => { this._ModalVisible({modalComment: false}) }}
                      transparent small style={mStyles.modalBtClose}>
                      <Icon name='times' size={20} style={mStyles.modalIcoClose} />
                    </Button>
                    <Image
                      source={require('../../Assets/Images/actions/actionComment.png')}
                      style={styles.smallModalImage} />
                    <Text style={styles.smallModalTitle}>Comentar</Text>
                    <View style={styles.smallModalLine} />
                    <Text style={styles.p}>{I18n.t('tutorial.three.comment')}</Text>
                  </View>
                </Modal>
              </TouchableOpacity>

            </View>

            <View style={styles.rowIcos}>

              <TouchableOpacity onPress={() => { this.setState({ modalDonate: true }) }}>
                <Image
                  source={require('../../Assets/Images/actions/actionDonate.png')}
                  style={styles.ico} />
                <Text style={styles.icoText}>Apoiar</Text>
                <Modal
                  isVisible={this.state.modalDonate}
                  onBackdropPress={() => { this._ModalVisible({modalDonate: false}) }}
                  style={styles.smallModalContainer}
              >
                  <View style={styles.smallModalContent}>
                    <Button onPress={() => { this._ModalVisible({modalDonate: false}) }}
                      transparent small style={mStyles.modalBtClose}>
                      <Icon name='times' size={20} style={mStyles.modalIcoClose} />
                    </Button>
                    <Image
                      source={require('../../Assets/Images/actions/actionDonate.png')}
                      style={styles.smallModalImage} />
                    <Text style={styles.smallModalTitle}>Apoiar</Text>
                    <View style={styles.smallModalLine} />
                    <Text style={styles.p}>{I18n.t('tutorial.three.donate')}</Text>
                    <Text style={styles.p}>{I18n.t('tutorial.three.donate1')}</Text>
                  </View>
                </Modal>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => { this.setState({ modalShare: true }) }}>
                <Image
                  source={require('../../Assets/Images/actions/actionShare.png')}
                  style={styles.ico} />
                <Text style={styles.icoText}>Compartilhar</Text>
                <Modal
                  isVisible={this.state.modalShare}
                  onBackdropPress={() => { this._ModalVisible({modalShare: false}) }}
                  style={styles.smallModalContainer}
              >
                  <View style={styles.smallModalContent}>
                    <Button onPress={() => { this._ModalVisible({modalShare: false}) }}
                      transparent small style={mStyles.modalBtClose}>
                      <Icon name='times' size={20} style={mStyles.modalIcoClose} />
                    </Button>
                    <Image
                      source={require('../../Assets/Images/actions/actionShare.png')}
                      style={styles.smallModalImage} />
                    <Text style={styles.smallModalTitle}>Compartilhar</Text>
                    <View style={styles.smallModalLine} />
                    <Text style={styles.p}>{I18n.t('tutorial.three.share')}</Text>
                    <Text style={styles.p}>{I18n.t('tutorial.three.share1')}</Text>
                  </View>
                </Modal>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => { this.setState({ modalLike: true }) }}>
                <Image
                  source={require('../../Assets/Images/actions/actionLike.png')}
                  style={styles.ico} />
                <Text style={styles.icoText}>Gostar</Text>
                <Modal
                  isVisible={this.state.modalLike}
                  onBackdropPress={() => { this._ModalVisible({modalLike: false}) }}
                  style={styles.smallModalContainer}
              >
                  <View style={styles.smallModalContent}>
                    <Button onPress={() => { this._ModalVisible({modalLike: false}) }}
                      transparent small style={mStyles.modalBtClose}>
                      <Icon name='times' size={20} style={mStyles.modalIcoClose} />
                    </Button>
                    <Image
                      source={require('../../Assets/Images/actions/actionLike.png')}
                      style={styles.smallModalImage} />
                    <Text style={styles.smallModalTitle}>Gostar</Text>
                    <View style={styles.smallModalLine} />
                    <Text style={styles.p}>{I18n.t('tutorial.three.like')}</Text>
                  </View>
                </Modal>
              </TouchableOpacity>

            </View>
            <Button
              rounded
              onPress={() => { Communications.web(faqUrl) }}
              style={{backgroundColor: '#52a34d', alignSelf: 'center', paddingHorizontal: 30 }}>
              <Text style={{color: '#fff'}}>Dúvidas?</Text>
            </Button>
          </View>

        </ScrollView>
      </View>
    )
  }
}
