import axios from 'axios'
import { AsyncStorage } from 'react-native'
import { apiurl } from './api.variables'

let headers = {}

AsyncStorage.getItem('apiToken', (err, token) => {
  if (err) {
    console.log(err)
  }
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
})

const getUsersByLevel = (level, token, limit = 10, skip = 0) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: `${apiurl}/users?level=${level}&limit=${limit}&skip=${skip}`,
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const sendCertificate = () => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${apiurl}/user/certificate`,
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

export {
  getUsersByLevel, sendCertificate
}
