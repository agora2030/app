import axios from 'axios'
import { AsyncStorage } from 'react-native'
import { apiurl } from './api.variables'

let headers = {}

AsyncStorage.getItem('apiToken', (err, token) => {
  if (err) {
    console.log(err)
  }
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
})

const endpointInitiatives = (limit = 10, skip = 0, scope, filter) => {
  let s = ''
  let f = ''
  if (scope) {
    s = `&scope=${scope}`
  }
  if (filter && (filter.q || filter.ods)) {
    if (!filter.q) {
      delete filter.q
    }
    if (!filter.ods || filter.ods === '0') {
      delete filter.ods
    }
    f = `&filter=${JSON.stringify(filter)}`
  }
  return `${apiurl}/initiatives/agora2030?limit=${limit}&skip=${skip}${s}${f}`
}

const endpointInitiative = (id) => {
  return `${apiurl}/initiatives/agora2030/${id}`
}
const endpointInitiativeMedias = (id, type) => {
  return `${apiurl}/initiatives/${id}/medias?type=${type}`
}
const endpointInitiativeUrls = (id, type) => {
  let limit = ''
  if (type === 'video') {
    limit = '&limit=1'
  }
  return `${apiurl}/initiatives/${id}/urls?type=${type}${limit}`
}
const endpointInitiativeAnswers = (id) => {
  return `${apiurl}/initiatives/${id}/answers?mobile=true`
}

const getInitiatives = (token, limit, skip, scope, filter) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: endpointInitiatives(limit, skip, scope, filter),
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}
const getInitiative = (token, id) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: endpointInitiative(id),
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}
const getInitiativeMedias = (token, id, type) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: endpointInitiativeMedias(id, type),
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}
const getInitiativeUrls = (token, id, type) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: endpointInitiativeUrls(id, type),
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}
const getInitiativeAnswers = (token, id) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: endpointInitiativeAnswers(id),
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

export {
  getInitiatives,
  getInitiative,
  getInitiativeMedias,
  getInitiativeAnswers,
  getInitiativeUrls
}
