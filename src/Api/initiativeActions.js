import axios from 'axios'
import { AsyncStorage } from 'react-native'
import { apiurl } from './api.variables'

let headers = {}

AsyncStorage.getItem('apiToken', (err, token) => {
  if (err) {
    console.log(err)
  }
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
})

const like = (initiativeId, terms, token) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
    headers['Content-Type'] = 'application/json'
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${apiurl}/initiatives/${initiativeId}/likes`,
      headers,
      data: {terms}
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const favorite = (initiativeId, token) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
    headers['Content-Type'] = 'application/json'
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${apiurl}/profile/favorites`,
      headers,
      data: {initiativeId}
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const donate = (initiativeId, value, token) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
    headers['Content-Type'] = 'application/json'
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${apiurl}/initiatives/${initiativeId}/donates`,
      headers,
      data: {value: parseInt(value)}
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const share = (initiativeId, token) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: `${apiurl}/initiatives/${initiativeId}/share`,
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

export {
  like, favorite, donate, share
}
