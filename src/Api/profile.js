import axios from 'axios'
import { AsyncStorage } from 'react-native'
import { apiurl } from './api.variables'

let headers = {}

AsyncStorage.getItem('apiToken', (err, token) => {
  if (err) {
    console.log(err)
  }
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
})

const endpointProfile = (scope) => {
  let s = ''
  if (scope) {
    s = `?scope=${scope}`
  }
  return `${apiurl}/profile${s}`
}
const endpointUserProfile = (scope, id) => {
  let s = ''
  if (scope) {
    s = `?scope=${scope}`
  }
  return `${apiurl}/users/${id}${s}`
}
const endpointActivities = (type = '', id) => {
  let t = ''
  if (type) {
    t = `?type=${type}`
  }
  return `${apiurl}/users/${id}/activities${t}`
}
const endpointProfileActivities = (type = '') => {
  let t = ''
  if (type) {
    t = `?type=${type}`
  }
  return `${apiurl}/profile/activities${t}`
}
const endpointAvatar = `${apiurl}/profile/setavatar`
const endpointRecoverPwd = `${apiurl}/recovery/password`

const getProfile = (scope, token) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: endpointProfile(scope),
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const getUserProfile = (scope, token, id) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: endpointUserProfile(scope, id),
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const getUserActivities = (token, type = '', id) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: endpointActivities(type, id),
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}
const getProfileActivities = (token, type = '') => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: endpointProfileActivities(type),
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const editProfile = (token, data) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'patch',
      url: endpointProfile(),
      data,
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const setProfilePicture = (token, file) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  let data = new FormData()
  data.append('file', {
    uri: file.uri,
    type: file.type,
    name: 'avatar'
  })
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: endpointAvatar,
      data,
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const recoverPassword = (email) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: endpointRecoverPwd,
      data: { email },
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

export {
  getProfile,
  getUserActivities,
  editProfile,
  setProfilePicture,
  recoverPassword,
  getUserProfile,
  getProfileActivities
}
