import axios from 'axios'
import { AsyncStorage } from 'react-native'
import { apiurl } from './api.variables'

let headers = {}

AsyncStorage.getItem('apiToken', (err, token) => {
  if (err) {
    console.log(err)
  }
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
})
const EP = {
  get: `${apiurl}/ods?limit=20`
}

const getOds = (token) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: EP.get,
      headers,
      crossDomain: true
    })
    .then(success => {
      resolve(success)
    })
    .catch(error => {
      reject(error)
    })
  })
}

export {
  getOds
}
