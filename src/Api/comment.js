import axios from 'axios'
import { AsyncStorage } from 'react-native'
import { apiurl } from './api.variables'

let headers = {}

AsyncStorage.getItem('apiToken', (err, token) => {
  if (err) {
    console.log(err)
  }
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
})

const getCommentsEndpoint = (initiativeId, limit = 20, skip = 0) => {
  return `${apiurl}/initiatives/${initiativeId}/comments?limit=${limit}&skip=${skip}`
}
const commentEndpoint = (initiaveId, commentId) => {
  return `${apiurl}/initiatives/${initiaveId}/comments/${commentId}`
}

const getComments = (token, initiativeId, limit, skip) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url: getCommentsEndpoint(initiativeId, limit, skip),
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const createComment = (token, initiativeId, data) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'post',
      url: getCommentsEndpoint(initiativeId),
      headers,
      data
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const updateComment = (token, initiativeId, commentId, text) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'patch',
      url: commentEndpoint(initiativeId, commentId),
      headers,
      data: { text }
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

const deleteComment = (token, initiativeId, commentId) => {
  if (token) {
    headers.Authorization = `Bearer ${token}`
  }
  return new Promise((resolve, reject) => {
    axios({
      method: 'delete',
      url: commentEndpoint(initiativeId, commentId),
      headers
    }).then(
      success => {
        resolve(success)
      },
      error => {
        reject(error)
      }
    )
  })
}

export {
  getComments,
  createComment,
  updateComment,
  deleteComment
}
