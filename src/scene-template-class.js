import React, { Component } from 'react'
import { Container, Content, Text } from 'native-base'

import AppHeader from '../../Components/app-header'

export default class Scene extends Component {
  render () {
    // const { navigate, goBack } = this.props.navigation
    return (
      <Container>

        <AppHeader {...this.props} title='Título' backButton />

        <Content>
          <Text>
          Component template (Class)
          </Text>
        </Content>

      </Container>
    )
  }
}
