import { AsyncStorage } from 'react-native'
import {
  SHOW_TUTORIAL
} from './AppconfigTypes.js'

const INITIAL_STATE = {
  showTutorial: true
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SHOW_TUTORIAL:
      AsyncStorage.setItem('showTutorial', JSON.stringify(!state.showTutorial))
      return { ...state, showTutorial: !state.showTutorial }

    default:
      return state
  }
}
