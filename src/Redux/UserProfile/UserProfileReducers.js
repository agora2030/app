import { AsyncStorage } from 'react-native'
import OneSignal from 'react-native-onesignal'

import {
  MODIFICA_RECEIVEPUSH,
  MODIFICA_RECEIVEEMAIL,
  GOOGLE_LOGIN,
  FACEBOOK_LOGIN,
  MODIFICA_API_TOKEN,
  UPDATE_USER_PROFILE
} from './UserProfileTypes'

const INITIAL_STATE = {
  receiveEmail: true,
  receivePush: true,
  apiToken: '',
  avatarUrls: {}
}

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case MODIFICA_RECEIVEPUSH:
      return { ...state, receivePush: !state.receivePush }

    case MODIFICA_RECEIVEEMAIL:
      return { ...state, receiveEmail: !state.receiveEmail }

    case MODIFICA_API_TOKEN:
      AsyncStorage.setItem('apiToken', action.payload.token, (err) => {
        if (err) {
          console.log('erro ao salvar token: ', err)
        }
        OneSignal.sendTag('id', action.payload.id)
        console.log('Token salvo!')
      })

      return { ...state, apiToken: action.payload.token }

    case GOOGLE_LOGIN:
      return {
        ...state,
        email: action.payload.email || '',
        socialAvatar: action.payload.photo || '',
        firstName: action.payload.givenName || '',
        lastName: action.payload.familyName || action.payload.givenName || '',
        socialLoginOrigin: 'google'
      }
    case FACEBOOK_LOGIN:
      return {
        ...state,
        email: action.payload.email || '',
        socialAvatar: action.payload.picture.data.url ? action.payload.picture.data.url + '?width=500&height=500' : '',
        firstName: action.payload.first_name || '',
        lastName: action.payload.last_name || action.payload.first_name || '',
        city: action.payload.hometown ? action.payload.hometown.name.split(',')[0] : '',
        gender: (action.payload.gender === 'male' ? 'Masculino' : 'Feminino') || '',
        birthDate: action.payload.birthday ? new Date(action.payload.birthday) : '',
        socialLoginOrigin: 'facebook'
      }
    case UPDATE_USER_PROFILE:
      // console.log('update user profile', action.payload)
      if (action.payload.token) {
        action.payload.apiToken = action.payload.token
        delete action.payload.token
        AsyncStorage.setItem('apiToken', action.payload.apiToken, () => {
        })
      }
      if (action.payload === 'logout') {
        AsyncStorage.removeItem('apiToken', () => {
          console.log('removido')
        })
        return {
          ...state,
          ...INITIAL_STATE
        }
      }
      return {
        ...state,
        ...action.payload
      }
    default:
      return state
  }
}
