import register from './register/ptbr'
import tutorial from './carouselstart/ptbr'
import mission from './mission/ptbr'

export default {
  register,
  tutorial,
  mission
}
