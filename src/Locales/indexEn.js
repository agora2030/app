import register from './register/en'
import tutorial from './carouselstart/en'
import mission from './mission/en'

export default {
  register,
  tutorial,
  mission
}
