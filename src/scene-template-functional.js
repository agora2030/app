import React from 'react'
import { Container, Content, Text } from 'native-base'

import AppHeader from '../../Components/app-header'

export default (props) => (
  <Container>

    <AppHeader {...props} title='Título' backButton />

    <Content>
      <Text>
      Component template (functional)
      </Text>
    </Content>

  </Container>
)
