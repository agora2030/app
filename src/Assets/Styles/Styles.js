import { Platform, StyleSheet } from 'react-native'

const colors = {
  primary: '#52a34d',
  secondary: '#56c02a',
  lightGreen: '#D1DCCE',
  one: '#e5233b',
  two: '#c08b2f',
  three: '#0997d9',
  four: '#00689d',
  five: '#a21a42',
  six: '#fdc30a',
  seven: '#c51a2d',
  eight: '#fd9d24',
  nine: '#dd1367',
  ten: '#fe6925'
}

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  row:{ 
    flexDirection: 'row'
  },
  boxWellcome: {
    backgroundColor: '#afda92',
    justifyContent: 'center',
    padding: 10
  },
  arrowWelcome: {
    fontSize: 34,
    textAlign: 'center'
  },
  title: {
    textAlign: 'center',
    marginBottom: 20
  },
  text: {
    textAlign: 'center',
    marginBottom: 10
  },
  missionSelectedContent: {
    backgroundColor: colors.secondary
  },
  missionSelectedContainer: {
    // alignItems: 'center'
  },
  missionScore: {
    backgroundColor: '#fff',
    padding: 25,
    paddingBottom: 40,
    paddingTop: 40
  },
  missionSelectedNow: {
    backgroundColor: colors.lightGreen,
    padding: 15,
    paddingTop: 25,
    paddingBottom: 25
  },

  missionDirectoryTitle: {
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  missionDirectoryBox: {
    marginTop: -70,
    zIndex: 10,
    width: null
  },
  missionDirectoryIcon: {
    color: '#fff',
    fontSize: 20,
    marginBottom: 5
  },
  textShadow: {
    color: '#fff',
    textShadowColor: '#000',
    textShadowRadius: 2,
    textShadowOffset: {
      width: 2,
      height: 2
    }
  },

  textCenter: {
    textAlign: 'center'
  },
  contentCenter: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  baseText: {
    color: '#000',
    fontSize: 15
  },
  boxHeader: {
    backgroundColor: colors.primary,
    paddingVertical: 25,
    paddingHorizontal: 20
  },
  boxHeaderIcon: {
    color: '#fff',
    fontSize: 28,
    marginRight: 10
  },
  boxHeaderTitle: {
    color: '#fff'
  },
  tagTop: {
    position: 'absolute',
    top: 180,
    left: 10,
    zIndex: 10
  },
  actionIcon: {
    fontSize: 28
    // padding: 18,
  },
  smallText: {
    fontSize: 12
  },
  smallTextCenter: {
    textAlign: 'center',
    marginVertical: 8
  },
  h3: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 12
  },
  label: {
    marginLeft: 6,
    marginTop: -2
  },
  input: {
    backgroundColor: '#fff',
    borderWidth: 1,
    borderRadius: 40,
    borderColor: '#ddd',
    marginBottom: 8,
    height: 45,
    paddingLeft: 15
  },
  TextInputMask: {
    flex: 1,
    borderBottomColor: 'transparent',
    borderWidth: 0
  },
  buttonPrimary: {
    backgroundColor: colors.primary,
    marginVertical: 15,
    marginHorizontal: 50,
    height: 40
  },
  btnDisabled: {
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#ccc',
    marginVertical: 15,
    marginHorizontal: 50,
    height: 40
  },
  textButtonPrimary: {
    color: '#fff',
    fontSize: 20,
    ...Platform.select({
      android: {
        paddingTop: 0,
        paddingBottom: 10
      },
      ios: {
        marginTop: -6
      }
    })
  },
  textButtonPrimaryDisabled: {
    color: '#ccc',
    fontSize: 20,
    ...Platform.select({
      android: {
        paddingTop: 0,
        paddingBottom: 10
      },
      ios: {
        marginTop: -6
      }
    })
  },
  ods: {
    paddingVertical: 2,
    paddingHorizontal: 6,
    marginRight: 10
  },
  odsId: {
    color: '#fff',
    fontSize: 14,
    fontWeight: 'bold',
    margin: 0,
    padding: 0,
    textAlign: 'center'
  },
  modalBtClose: {
    alignSelf: 'flex-end',
    borderWidth: 1,
    borderColor: '#ccc',
    width: 35,
    height: 35,
    borderRadius: 35,
    backgroundColor: '#fff',
    justifyContent: 'center',
    marginRight: -15,
    marginTop: -15,
    ...Platform.select({
      android: {
        // marginTop: 0
      }
    })
  },
  modalIcoClose: {
    color: 'rgba(0,0,0,.5)',
    ...Platform.select({
      android: {
        marginTop: -2
      },
      ios: {
        marginTop: -2
      }
    })
  }
})
