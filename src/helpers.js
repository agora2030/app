function youtubeParser (itens) {
  if (itens.length === 0) {
    return false
  }
  const url = itens[0].url
  const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/
  const match = url.match(regExp)
  return (match && match[7].length === 11) ? match[7] : false
}

function getMissionIcon (title) {
  const icons = {
    Pessoas: require('./Assets/Images/missions/pessoas.png'),
    Planeta: require('./Assets/Images/missions/planeta.png'),
    Prosperidade: require('./Assets/Images/missions/prosperidade.png'),
    Parceria: require('./Assets/Images/missions/parceria.png'),
    Paz: require('./Assets/Images/missions/paz.png'),
    Curadoria: require('./Assets/Images/missions/curadoria.png'),
    Pessoas_disabled: require('./Assets/Images/missions/pessoas_disabled.png'),
    Planeta_disabled: require('./Assets/Images/missions/planeta_disabled.png'),
    Prosperidade_disabled: require('./Assets/Images/missions/prosperidade_disabled.png'),
    Parceria_disabled: require('./Assets/Images/missions/parceria_disabled.png'),
    Paz_disabled: require('./Assets/Images/missions/paz_disabled.png'),
    Curadoria_disabled: require('./Assets/Images/missions/curadoria_disabled.png')
  }
  return icons[title]
}

function getActionIcon (action) {
  const icons = {
    actionLike: require('./Assets/Images/actions/actionLike.png'),
    actionView: require('./Assets/Images/actions/actionView.png'),
    actionShare: require('./Assets/Images/actions/actionShare.png'),
    actionDonate: require('./Assets/Images/actions/actionDonate.png'),
    actionFavorite: require('./Assets/Images/actions/actionFavorite.png'),
    actionComment: require('./Assets/Images/actions/actionComment.png')
  }
  return icons[action]
}

function getMapFull (mission = '') {
  let mapaAtual = 'inicial'

  if (mission) {
    mapaAtual = mission
  }

  const mapasFull = {
    inicial: require('./Assets/Images/missions/mapa_inicial.png'),
    Pessoas: require('./Assets/Images/missions/mapa_pessoas.png'),
    Planeta: require('./Assets/Images/missions/mapa_planetas.png'),
    Parceria: require('./Assets/Images/missions/mapa_parcerias.png'),
    Prosperidade: require('./Assets/Images/missions/mapa_prosperidade.png'),
    Paz: require('./Assets/Images/missions/mapa_paz.png'),
    Curadoria: require('./Assets/Images/missions/mapa_completo.png')
  }

  return mapasFull[mapaAtual]
}

function getMissionBg (mission) {
  switch (mission) {
    case 'Pessoas':
      return require('./Assets/Images/missions/pessoas_bg.png')
    case 'Planeta':
      return require('./Assets/Images/missions/planeta_bg.png')
    case 'Prosperidade':
      return require('./Assets/Images/missions/prosperidade_bg.png')
    case 'Parceria':
      return require('./Assets/Images/missions/parceria_bg.png')
    case 'Paz':
      return require('./Assets/Images/missions/paz_bg.png')
    case 'Curadoria':
      return require('./Assets/Images/missions/curadoria_bg.png')
    default:
      return require('./Assets/Images/missions/inicio_bg.png')
  }
}

function getUserAvatar (user) {
  const defaultAvatar = require('./Assets/Images/avatar_default.jpg')
  if (user.socialAvatar) {
    return {uri: user.socialAvatar}
  } else if (user.avatarUrls) {
    return (user.avatarUrls['150x150']) ? {uri: user.avatarUrls['150x150']} : defaultAvatar
  }
  return defaultAvatar
}

function formatDate (date) {
  const dt = new Date(date)
  return `${dt.getDate()}/${dt.getMonth() + 1}/${dt.getFullYear()}`
}

function getUserNivelName (nivel) {
  switch (nivel) {
    case 1:
      return 'Nível Acampamento'
    case 2:
      return 'Nível Casa'
    case 3:
      return 'Nível Comunidade'
    case 4:
      return 'Nível País'
    case 5:
      return 'Nível mundo'
    default:
      return 'Nível Acampamento'
  }
}

function progressBarPercent (user) {
  if (!user.xp) {
    return 0
  }
  const progress = user.xp / getLimitXpByNivel(user.nivel)
  return (progress > 1) ? 1 : Math.round(progress * 10) / 10
}

function userNivelColor (nivel) {
  switch (nivel) {
    case 1:
      return '#cba46c'
    case 2:
      return '#e6963d'
    case 3:
      return '#f0c82a'
    case 4:
      return '#50d5b6'
    case 5:
      return '#35bb2f'
    default:
      return '#cba46c'
  }
}

function getLimitXpByNivel (nivel) {
  switch (nivel) {
    case 1:
      return 50
    case 2:
      return 150
    case 3:
      return 300
    case 4:
      return 900
    case 5:
      return 900
    default:
      return 50
  }
}

function deleteEmptyKeys (obj) {
  let o = Object.assign({}, obj)
  for (let key in o) {
    if (o[key] == null || typeof o[key] === 'undefined' || o[key].length === 0) {
      delete o[key]
    }
  }
  return o
}

export {
  youtubeParser, getMissionIcon, getActionIcon, userNivelColor, deleteEmptyKeys,
  getUserAvatar, formatDate, progressBarPercent, getUserNivelName, getMissionBg,
  getMapFull
}
