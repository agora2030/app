//
//  AuthApp-Bridging-Header.h
//  AuthApp
//
//  Created by Assunção Jr on 09/11/17.
//  Copyright © 2017 Facebook. All rights reserved.
//

#ifndef AppAgora2030_Bridging_Header_h
#define AppAgora2030_Bridging_Header_h

#import <React/RCTBridgeModule.h>
#import <React/RCTViewManager.h>
#import <React/RCTEventEmitter.h>
#import <GoogleSignIn/GoogleSignIn.h>

#endif /* AppAgora2030_Bridging_Header_h */
